



##### [Back to Platform docs](./README.md)

## Analytics Methods
This node js service provides 3 servers, Platform - which contains APIs to power platform analytics. Internal which contains APIs to interact with BQ and Admin which contains APIs to power dunzo dashboard analytics


Default
* [createScheduler](#createscheduler)
* [getTemplateDetailsByFilters](#gettemplatedetailsbyfilters)
* [fetchSidebarDropdownValues](#fetchsidebardropdownvalues)
* [getGenericDashboardTableRespModel](#getgenericdashboardtablerespmodel)
* [getGenericReportTableRespModel](#getgenericreporttablerespmodel)
* [getDistinctFields](#getdistinctfields)
* [getLayoutByName](#getlayoutbyname)
* [getLayoutPermissions](#getlayoutpermissions)
* [getSalesDumpFilters](#getsalesdumpfilters)
* [getSchedulerHistory](#getschedulerhistory)
* [getSchedulerDetails](#getschedulerdetails)
* [getDashboardStatByName](#getdashboardstatbyname)
* [downloadReportTableData](#downloadreporttabledata)
* [downloadReportTableDataSync](#downloadreporttabledatasync)
* [getReportData](#getreportdata)
* [createTemplate](#createtemplate)
* [getTemplateLayoutById](#gettemplatelayoutbyid)
* [fetchSchedulerById](#fetchschedulerbyid)
* [updateScheduler](#updatescheduler)
* [deleteScheduler](#deletescheduler)
* [updateTemplateById](#updatetemplatebyid)
* [deleteTemplateByID](#deletetemplatebyid)
* [duplicateTemplate](#duplicatetemplate)
* [validateTemplateName](#validatetemplatename)
* [fetchLayoutByTemplateName](#fetchlayoutbytemplatename)
* [fetchSliderValues](#fetchslidervalues)
* [fetchBarGraphData](#fetchbargraphdata)
* [fetchLineGraphData](#fetchlinegraphdata)
* [fetchLineDataComparisionGraph](#fetchlinedatacomparisiongraph)
* [fetchProgressBarGraphData](#fetchprogressbargraphdata)
* [fetchStatData](#fetchstatdata)
* [cancelExportJob](#cancelexportjob)




## Methods with example and description



#### createScheduler
Create scheduler




```swift
platformClient.analytics.createScheduler(body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- |
| body | ScheduleRequestBody | yes | Request body |


This API creates scheduler and email along with template and destination mapping in DB Sends email to user is this is once scheduler

*Returned Response:*




[SchedulerResponseBody](#SchedulerResponseBody)

Returns message for successful email scheduling




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "message": "Scheduler creation started successfully"
  }
}
```
</details>

<details>
<summary><i>&nbsp; success_email</i></summary>

```json
{
  "value": {
    "message": "Scheduler creation started successfully"
  }
}
```
</details>

</details>









---


#### getTemplateDetailsByFilters
Fetch template details




```swift
platformClient.analytics.getTemplateDetailsByFilters(body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- |
| body | TemplateDetailsRequestBody | yes | Request body |


Returns template details for a company and provided filters

*Returned Response:*




[ReportsDetailsRespModel](#ReportsDetailsRespModel)

Returns template details by filters and sorting




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "headers": [
      {
        "config": {
          "enable_select_all": true,
          "sort": true,
          "type": "date"
        },
        "display_name": "Name",
        "name": "name"
      }
    ],
    "records": [
      {
        "values": {
          "id": {
            "value": 11736
          },
          "name": {
            "value": "New User Vs Returning User"
          },
          "seq_num": {
            "value": 0
          },
          "category": {
            "type": "info",
            "value": "USERS"
          },
          "last_viewed": {
            "value": "1696918754925"
          },
          "user_name": {
            "value": "FYND"
          },
          "config": {
            "value": {
              "selected_columns": [
                {
                  "config": {
                    "sort": "true,",
                    "style": {
                      "max_length": 25
                    }
                  },
                  "display_name\"": "User Id",
                  "name": "fynd_user_id"
                }
              ]
            }
          },
          "type": {
            "value": "report_new_vs_returning"
          },
          "is_default": {
            "value": true
          },
          "action": {
            "icons": [
              {
                "name": "schedule",
                "action": "schedule"
              },
              {
                "name": "email-grey",
                "action\"": "email"
              }
            ]
          }
        }
      }
    ],
    "page": {
      "current": 1,
      "size": 25,
      "item_total": 4,
      "total_page": 1,
      "has_next": false,
      "has_previous": false
    }
  }
}
```
</details>

</details>









---


#### fetchSidebarDropdownValues
Fetch text value pair to fill in dropdown




```swift
platformClient.analytics.fetchSidebarDropdownValues(category: category, dropdownName: dropdownName, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| dropdownName | String | yes |  |  
| body | DropdownRequest | yes | Request body |


Fetch text value pair to fill in dropdown

*Returned Response:*




[[DropdownResponse]](#[DropdownResponse])

Fetch text value pair to fill in dropdown




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": [
    {
      "text": "test",
      "value": "test"
    }
  ]
}
```
</details>

</details>









---


#### getGenericDashboardTableRespModel
Generic dashboard table response model




```swift
platformClient.analytics.getGenericDashboardTableRespModel(category: category, graphName: graphName, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| graphName | String | yes |  |  
| body | TableRequestBody | yes | Request body |


This helps to get a generic dashboard table response model which includes growth percentages alog with each row

*Returned Response:*




[TableResponseModel](#TableResponseModel)

Returns dashboard table response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "rows": [
      {
        "date": "Oct 11, 2023",
        "shipments": 11,
        "inTATPercent": 9.09,
        "pending": 10
      }
    ],
    "page": {
      "current": 1,
      "size": 10,
      "item_total": 4,
      "total_page": 1,
      "has_next": false,
      "has_previous": false
    }
  }
}
```
</details>

</details>









---


#### getGenericReportTableRespModel
Platform report table Copy




```swift
platformClient.analytics.getGenericReportTableRespModel(category: category, graphName: graphName, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| graphName | String | yes |  |  
| body | TableRequestBody | yes | Request body |


This helps to fetch response for report table

*Returned Response:*




[TableResponseModel](#TableResponseModel)

Returns dashboard table response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "rows": [
      {
        "date": "Oct 11, 2023",
        "shipments": 11,
        "inTATPercent": 9.09,
        "pending": 10
      }
    ],
    "page": {
      "current": 1,
      "size": 10,
      "item_total": 4,
      "total_page": 1,
      "has_next": false,
      "has_previous": false
    }
  }
}
```
</details>

</details>









---


#### getDistinctFields
Get distinct reports filter values by name




```swift
platformClient.analytics.getDistinctFields(fieldName: fieldName) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| fieldName | String | yes |  |  



Returns distinct values for provide filter name

*Returned Response:*




[[FilterRespBody]](#[FilterRespBody])

List of distinct filters




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": [
    {
      "text": "test",
      "value": "test"
    }
  ]
}
```
</details>

</details>









---


#### getLayoutByName
Get analytics layout




```swift
platformClient.analytics.getLayoutByName(layoutName: layoutName) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| layoutName | String | yes |  |  



Get analytics layout

*Returned Response:*




[LayoutResponseModel](#LayoutResponseModel)

Analytics layout response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "id": 1,
    "company": "default",
    "user_id": "default",
    "layout_config": {
      "layout_details": {
        "data_source": "permissions",
        "orders": {
          "data_source": "dashboard_orders"
        },
        "inventory": {
          "data_source": "dashboard_inventory"
        },
        "logistics": {
          "data_source": "dashboard_logistics"
        },
        "finance": {
          "data_source": "dashboard_finance"
        },
        "users": {
          "data_source": "dashboard_users"
        }
      },
      "global_filters": [
        {
          "id": "sales_channel",
          "name": "Sales Channel",
          "placeholder": "Select channel",
          "searchable": false,
          "show_collapse": false,
          "show_tags": false,
          "show_clear": false,
          "is_default_required": true,
          "data_source": true,
          "select_all_enabled": true,
          "default_all_selected": true,
          "is_multiselect": true,
          "reset": true
        },
        {
          "id": "date",
          "name": "date",
          "show_compare": true,
          "allowed_shortcuts": [
            "lastWeek",
            "lastMonth",
            "last3months",
            "last6months"
          ]
        },
        {
          "id": "time_frame_clause",
          "name": "Time Filter",
          "placeholder": "Select time filter",
          "searchable": false,
          "show_collapse": false,
          "show_tags": false,
          "show_clear": false,
          "type": "SINGLE_SELECT_DROPDOWN",
          "range": [
            {
              "text": "Hourly",
              "value": "hourly",
              "min_duration": 0,
              "max_duration": 2
            },
            {
              "text": "Daily",
              "value": "daily",
              "min_duration": 0
            },
            {
              "text": "Weekly",
              "value": "weekly",
              "min_duration": 8
            },
            {
              "text": "Monthly",
              "value": "monthly",
              "min_duration": 31
            },
            {
              "text": "Quarterly",
              "value": "quarterly",
              "min_duration": 92
            }
          ]
        }
      ]
    },
    "layout_name": "dashboard_analytics",
    "is_default": true
  }
}
```
</details>

</details>









---


#### getLayoutPermissions
Get analytics layout permissions




```swift
platformClient.analytics.getLayoutPermissions(formatting: formatting) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| formatting | Bool? | no |  |  



Get analytics layout permissions

*Returned Response:*




[PermissionResponse](#PermissionResponse)

Layout permission response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": [
    "test"
  ]
}
```
</details>

</details>









---


#### getSalesDumpFilters
Get sales dump filters




```swift
platformClient.analytics.getSalesDumpFilters() { (response, error) in
    // Use response
}
```






Get sales dump filters

*Returned Response:*




[[String: Any]](#[String: Any])

Sales dump filter response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "Unfulfilled": [
      "New"
    ],
    "Processed": [
      "In Transit"
    ],
    "Return": [
      "Return Initiated"
    ]
  }
}
```
</details>

</details>









---


#### getSchedulerHistory
Get Scheduler history




```swift
platformClient.analytics.getSchedulerHistory(schedulerId: schedulerId) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| schedulerId | Int | yes |  |  



Get Scheduler history

*Returned Response:*




[[SchedulerHistoryResponse]](#[SchedulerHistoryResponse])

Scheduler history response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": [
    {
      "type": "edit-history",
      "key": "Scheduler has been updated by",
      "value": "item.created_by",
      "created_on": "item.created_at"
    }
  ]
}
```
</details>

</details>









---


#### getSchedulerDetails
Get Scheduler details




```swift
platformClient.analytics.getSchedulerDetails(templateId: templateId) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| templateId | Int | yes |  |  



Get Scheduler details

*Returned Response:*




[SchedulerDetailsRespModel](#SchedulerDetailsRespModel)

Scheduler details response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "headers": [
      {
        "config": {
          "enable_select_all": true,
          "sort": true,
          "type": "date"
        },
        "display_name": "Name",
        "name": "name"
      }
    ],
    "records": [
      {
        "values": {
          "id": {
            "value": 11736
          },
          "name": {
            "value": "New User Vs Returning User"
          },
          "seq_num": {
            "value": 0
          },
          "category": {
            "type": "info",
            "value": "USERS"
          },
          "last_viewed": {
            "value": "1696918754925"
          },
          "user_name": {
            "value": "FYND"
          },
          "config": {
            "value": {
              "selected_columns": [
                {
                  "config": {
                    "sort": "true,",
                    "style": {
                      "max_length": 25
                    }
                  },
                  "display_name\"": "User Id",
                  "name": "fynd_user_id"
                }
              ]
            }
          },
          "type": {
            "value": "report_new_vs_returning"
          },
          "is_default": {
            "value": true
          },
          "action": {
            "icons": [
              {
                "name": "schedule",
                "action": "schedule"
              },
              {
                "name": "email-grey",
                "action\"": "email"
              }
            ]
          }
        }
      }
    ]
  }
}
```
</details>

</details>









---


#### getDashboardStatByName
Fetch stat widget for dashboard




```swift
platformClient.analytics.getDashboardStatByName(graphName: graphName, category: category, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| graphName | String | yes |  |   
| category | String | yes |  |  
| body | StatFiltersRequest | yes | Request body |


Fetch stat widget for dashboard

*Returned Response:*




[StatFiltersResponse](#StatFiltersResponse)

Returns template details by filters and sorting




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "items": [
      {
        "id": "rto"
      },
      {
        "actual_value": 0
      },
      {
        "change_in_value": 0
      },
      {
        "trend_mapping": "green"
      }
    ]
  }
}
```
</details>

</details>









---


#### downloadReportTableData
Download table data for report




```swift
platformClient.analytics.downloadReportTableData(graphName: graphName, category: category, exportType: exportType, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| graphName | String | yes |  |   
| category | String | yes |  |   
| exportType | String | yes |  |  
| body | ReportFiltersRequest | yes | Request body |


Download table data for report

*Returned Response:*




[String](#String)

Returns csv/excel file containing data




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": "order_61_reportreturn_amount_1697599414002"
}
```
</details>

</details>









---


#### downloadReportTableDataSync
Download table data for report in sync




```swift
platformClient.analytics.downloadReportTableDataSync(tableName: tableName, affiliateId: affiliateId, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| tableName | String | yes |  |   
| affiliateId | String | yes |  |  
| body | ExportRequest | yes | Request body |


Download table data for report in sync

*Returned Response:*




[String](#String)

Returns csv/excel file containing data




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": "order_61_reportreturn_amount_1697599414002"
}
```
</details>

</details>









---


#### getReportData
Fetch table data for records




```swift
platformClient.analytics.getReportData(tableName: tableName, affiliateId: affiliateId, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| tableName | String | yes |  |   
| affiliateId | String | yes |  |  
| body | ExportRequest | yes | Request body |


Fetch table data for records

*Returned Response:*




[[String: Any]](#[String: Any])

Fetch table data for records




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "rows": [
      {
        "totalRowCount": 100
      }
    ],
    "page": {
      "current": 1,
      "size": 10,
      "item_total": 100,
      "total_page": 10,
      "has_next": true,
      "has_previous": false
    }
  }
}
```
</details>

</details>









---


#### createTemplate
Create report template with applied filters




```swift
platformClient.analytics.createTemplate(body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- |
| body | SaveTemplateRequest | yes | Request body |


Create report template with applied filters

*Returned Response:*




[SaveTemplateResponse](#SaveTemplateResponse)

Response received when template is saved




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "seq_num": 1,
    "id": 5221,
    "category": "LOGISTICS",
    "report_type": "report_delay_delivery",
    "report_name": "Delay Delivery test 2",
    "company": "1",
    "user_name": "Fynd App",
    "user_id": "app@fynd.com",
    "created_at": "1697620150032",
    "updated_at": "1697620150032",
    "config": {
      "filters": {
        "sidebar_filters": [
          {
            "id": "store_code",
            "value": [
              {
                "text": "New  Test store edited1 (qwerty edited)",
                "value": "qwerty edited"
              }
            ],
            "data_type": "string"
          },
          {
            "id": "values",
            "value": "total_values"
          }
        ],
        "conditional_filters": [],
        "global_filters": {
          "sales_channel": [
            "{\"id\":\"64af877aea52c9414e6c510e\",\"is_marketplace\":false}"
          ],
          "end_date": "2023-10-18T18:29:59+0000",
          "start_date": "2023-10-15T18:30:00+0000",
          "shortcut": "weekToDate",
          "time_frame_clause": "daily",
          "compare_start_date": "2023-10-12T18:30:00+0000",
          "compare_end_date": "2023-10-15T18:29:59+0000"
        }
      },
      "selected_columns": [
        {
          "config": {
            "sort": true,
            "style": {
              "max_length": 25
            }
          },
          "display_name": "Date",
          "name": "state_date"
        }
      ]
    }
  }
}
```
</details>

</details>









---


#### getTemplateLayoutById
Fetch template layout




```swift
platformClient.analytics.getTemplateLayoutById(templateId: templateId) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| templateId | Int | yes |  |  



Return template layout details for a given company and template id

*Returned Response:*




[TemplateLayoutRespModel](#TemplateLayoutRespModel)

Return layout template layout by id




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "id": 470,
    "name": "Page Views",
    "report_type": "report_page_views",
    "category": "USERS",
    "is_editable": false,
    "layout": {
      "id": 791,
      "company": "default",
      "user_id": "default",
      "layout_config": {
        "category": "USERS",
        "display_name": "Page Views",
        "report_name": "report_page_views",
        "id": "",
        "table_info": {
          "datasource": "report/users/table/report_user_journey",
          "columns": [
            {
              "config": {
                "sort": true,
                "default_column": true,
                "prefix": "price",
                "format": true
              },
              "display_name": "Avg Purchase Value",
              "name": "avg_purchase_value"
            }
          ],
          "selected_columns": [
            {
              "config": {
                "sort": true,
                "default_column": true,
                "prefix": "price",
                "format": true
              },
              "display_name": "Avg Purchase Value",
              "name": "avg_purchase_value"
            }
          ]
        },
        "sidebar_filters": [],
        "conditional_filters_data": {
          "rows": [
            {
              "column_name": "fynd_user_id",
              "type": "string",
              "datasource": "",
              "name": "Fynd User Id"
            }
          ],
          "limit": 5
        },
        "report_filters": [],
        "global_filters": [
          {
            "id": "sales_channel",
            "name": "Sales Channel",
            "placeholder": "Select channel",
            "searchable": false,
            "show_collapse": false,
            "show_tags": false,
            "show_clear": false,
            "is_default_required": true,
            "data_source": true,
            "select_all_enabled": true,
            "default_all_selected": true,
            "is_multiselect": true,
            "reset": true
          },
          {
            "id": "date",
            "name": "date",
            "show_compare": false,
            "allowed_shortcuts": [
              "lastWeek",
              "lastMonth",
              "last3months",
              "last6months",
              "last7days"
            ]
          },
          {
            "id": "time_frame_clause",
            "name": "Time Filter",
            "placeholder": "Select time filter",
            "searchable": false,
            "is_hidden": true,
            "show_collapse": false,
            "show_tags": false,
            "show_clear": false,
            "type": "SINGLE_SELECT_DROPDOWN",
            "range": [
              {
                "text": "Hourly",
                "value": "hourly",
                "min_duration": 0,
                "max_duration": 1
              },
              {
                "text": "Daily",
                "value": "daily",
                "min_duration": 1
              },
              {
                "text": "Weekly",
                "value": "weekly",
                "min_duration": 8
              },
              {
                "text": "Monthly",
                "value": "monthly",
                "min_duration\"": 31
              },
              {
                "text": "Quarterly",
                "value": "quarterly",
                "min_duration": 92
              }
            ]
          }
        ]
      },
      "layout_name": "report_page_views",
      "is_default": true
    },
    "config": {
      "selected_columns": [
        {
          "config": {
            "sort": true,
            "style": {
              "max_length": 25
            },
            "default_column": true
          },
          "display_name": "Page",
          "name": "page"
        }
      ]
    }
  }
}
```
</details>

</details>









---


#### fetchSchedulerById
Fetch scheduler by id




```swift
platformClient.analytics.fetchSchedulerById(schedulerId: schedulerId) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| schedulerId | Int | yes |  |  



Fetch scheduler details using scheduler id

*Returned Response:*




[TemplateResponseModel](#TemplateResponseModel)

Fetch scheduler details by scheduler id




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "execution_details": {
      "last_run_at": 1697118720002,
      "next_run_at": 1697723520000
    },
    "scheduler_details": {
      "id": 24,
      "status": "active",
      "scheduler_id": "6527f9ae5f59ba089a302c2d",
      "company_id": "1",
      "created_at": "1697118638120",
      "updated_at": "1697118638120",
      "cron_expression": "00 52 13 * * 4",
      "destination_type": "EMAIL",
      "destination_id": 24,
      "frequency": "Weekly",
      "config": {
        "user_name": "sandbox user",
        "frequency": "Weekly",
        "day_or_date": [
          4
        ],
        "time": "07:22 pm",
        "user_id": {
          "_id": "39b6625fd0889bd79797ff96"
        },
        "tz_identifier": "Asia/Kolkata"
      },
      "email_detail\"": {
        "id": 24,
        "to": [
          "hridaynarayanvishwakarma@gofynd.com"
        ],
        "cc": [
          "hridaynarayanvishwakarma@gofynd.com"
        ],
        "bcc": [
          "hridaynarayanvishwakarma@gofynd.com"
        ],
        "subject": "User Journey Custom 12 Oct 719 PM",
        "body": "Weekly schedule Parth Sandbox\nEvery thursday at 722 PM \nWeek to Date",
        "attachment_type": "csv",
        "report_templates": [
          {
            "id": 24515,
            "seq_num": 15,
            "report_name": "User Journey Custom 12 Oct 719 PM",
            "config": {
              "filters": {
                "sidebar_filters": [],
                "conditional_filters": [],
                "global_filter": {
                  "end_date": "2023-10-12T18:29:59+0000",
                  "start_date": "2023-10-08T18:30:00+0000",
                  "shortcut": "weekToDate",
                  "sales_channel\"": [
                    "{\"id\":\"6524ceebf59a7c62e74049b0\",\"is_marketplace\":false}"
                  ],
                  "time_frame_clause": "daily",
                  "compare_start_date": "2023-10-04T18:30:00+0000",
                  "compare_end_date": "2023-10-08T18:29:59+0000"
                }
              }
            },
            "selected_columns\"": [
              {
                "config": {
                  "sort": true,
                  "style": {
                    "max_length": 40
                  },
                  "display_name": "Username",
                  "name": "user_name"
                }
              }
            ],
            "report_type": "report_user_journey",
            "category": "USERS"
          }
        ]
      }
    }
  }
}
```
</details>

</details>









---


#### updateScheduler
Update scheduler




```swift
platformClient.analytics.updateScheduler(schedulerId: schedulerId, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| schedulerId | Int | yes |  |  
| body | ScheduleRequestBody | yes | Request body |


This API updates scheduler details in DB and its scheduler in trigger-happy

*Returned Response:*




[SchedulerResponseBody](#SchedulerResponseBody)

Returns message for successful scheduler update




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "message": "Scheduler details with id 1 updated successfully"
  }
}
```
</details>

</details>









---


#### deleteScheduler
Delete scheduler




```swift
platformClient.analytics.deleteScheduler(schedulerId: schedulerId) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| schedulerId | Int | yes |  |  



This API deletes scheduler from DB and its scheduler from trigger-happy

*Returned Response:*




[SchedulerResponseBody](#SchedulerResponseBody)

Returns message for successful scheduler delete




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "message": "Scheduler details with id 1 deleted successfully"
  }
}
```
</details>

</details>









---


#### updateTemplateById
Update template details by id




```swift
platformClient.analytics.updateTemplateById(templateId: templateId, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| templateId | Int | yes |  |  
| body | SaveTemplateResponse | yes | Request body |


Update template details by id

*Returned Response:*




[[String: Any]](#[String: Any])

Response received after updating template




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "message": "Template updated successfully"
  }
}
```
</details>

</details>









---


#### deleteTemplateByID
Delete template by template id




```swift
platformClient.analytics.deleteTemplateByID(templateId: templateId) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| templateId | Int | yes |  |  



Delete template by template id

*Returned Response:*




[[String: Any]](#[String: Any])

Response reveived after deleting template by template id




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "message": "Template with id 1 deleted successfully"
  }
}
```
</details>

</details>









---


#### duplicateTemplate
Make a copy of template from another template




```swift
platformClient.analytics.duplicateTemplate(defaultTemplateName: defaultTemplateName, renameTo: renameTo) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| defaultTemplateName | String | yes |  |   
| renameTo | String | yes |  |  



Make a copy of template from another template

*Returned Response:*




[SaveTemplateResponse](#SaveTemplateResponse)

Make a copy of template from another template




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "seq_num": 1,
    "id": 1,
    "category": "LOGISTICS",
    "report_type": "report_delay_delivery",
    "report_name": "Delay Delivery test 21",
    "company": "1",
    "user_name": "Fynd App",
    "user_id": "app@fynd.com",
    "created_at": "1697620150032",
    "updated_at": "1697620150032",
    "config": {
      "filters": {
        "sidebar_filters": [
          {
            "id": "store_code",
            "value": [
              {
                "text": "New  Test store edited1 (qwerty edited)",
                "value": "qwerty edited"
              }
            ],
            "data_type": "string"
          },
          {
            "id": "values",
            "value": "total_values"
          }
        ],
        "conditional_filters": [],
        "global_filters": {
          "sales_channel": [
            "{\"id\":\"64af877aea52c9414e6c510e\",\"is_marketplace\":false}"
          ],
          "end_date": "2023-10-18T18:29:59+0000",
          "start_date": "2023-10-15T18:30:00+0000",
          "shortcut": "weekToDate",
          "time_frame_clause": "daily",
          "compare_start_date": "2023-10-12T18:30:00+0000",
          "compare_end_date": "2023-10-15T18:29:59+0000"
        }
      }
    }
  }
}
```
</details>

</details>









---


#### validateTemplateName
Check if template name is valid and unique




```swift
platformClient.analytics.validateTemplateName(templateName: templateName) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| templateName | String | yes |  |  



Check if template name is valid and unique

*Returned Response:*




[[String: Any]](#[String: Any])

Check if template name is valid and unique




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "is_valid": false,
    "message": "Template with name RTO already exists for company id 123"
  }
}
```
</details>

</details>









---


#### fetchLayoutByTemplateName
Fetch layout from template name




```swift
platformClient.analytics.fetchLayoutByTemplateName(templateName: templateName) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| templateName | String | yes |  |  



Fetch layout from template name

*Returned Response:*




[TemplateLayoutRespModel](#TemplateLayoutRespModel)

Response received when layout is fetched from template name




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "is_valid": false,
    "message": "Template with name RTO already exists for company id 123"
  }
}
```
</details>

</details>









---


#### fetchSliderValues
Fetch Slider value for provided filters




```swift
platformClient.analytics.fetchSliderValues(category: category, sliderName: sliderName, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| sliderName | String | yes |  |  
| body | SideBarRequest | yes | Request body |


Fetch Slider value for provided filters

*Returned Response:*




[SideBarResponse](#SideBarResponse)

Fetch Slider value for provided filters




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": [
    10
  ]
}
```
</details>

</details>









---


#### fetchBarGraphData
Fetch bar graph data for provided filters




```swift
platformClient.analytics.fetchBarGraphData(category: category, graphName: graphName, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| graphName | String | yes |  |  
| body | DashboardFilterRequest | yes | Request body |


Fetch bar graph data for provided filters

*Returned Response:*




[[String: Any]](#[String: Any])

Fetch bar graph data for provided filters




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "x_axis": [
      "₹0- ₹500",
      "₹501- ₹1000",
      "₹1001- ₹1500",
      "₹1501- ₹2000",
      "₹2000+"
    ],
    "y_axis": [
      {
        "max_value": 20009,
        "data": [
          20009,
          10,
          2,
          8,
          25
        ]
      }
    ],
    "tooltip_text": [
      [
        "₹0- ₹500",
        "20009 Products"
      ],
      [
        "₹501- ₹1000",
        "10 Products"
      ],
      [
        "₹1001- ₹1500",
        "2 Products"
      ],
      [
        "₹1501- ₹2000",
        "8 Products"
      ],
      [
        "₹2000+",
        "25 Products"
      ]
    ]
  }
}
```
</details>

</details>









---


#### fetchLineGraphData
Fetch line graph data for provided filters




```swift
platformClient.analytics.fetchLineGraphData(category: category, graphName: graphName, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| graphName | String | yes |  |  
| body | DashboardFilterRequest | yes | Request body |


Fetch line graph data for provided filters

*Returned Response:*




[[String: Any]](#[String: Any])

Fetch line graph data for provided filters




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "x_axis": [
      [
        "₹0- ₹500",
        "₹501- ₹1000"
      ],
      [
        "₹1001- ₹1500",
        "₹1501- ₹2000"
      ],
      [
        "₹2000+"
      ]
    ],
    "y_axis": [
      {
        "max_value": 20009,
        "data": [
          20009,
          10,
          2,
          8,
          25
        ]
      }
    ],
    "tooltip_text": [
      [
        "₹0- ₹500",
        "20009 Products"
      ],
      [
        "₹501- ₹1000",
        "10 Products"
      ],
      [
        "₹1001- ₹1500",
        "2 Products"
      ],
      [
        "₹1501- ₹2000",
        "8 Products"
      ],
      [
        "₹2000+",
        "25 Products"
      ]
    ]
  }
}
```
</details>

</details>









---


#### fetchLineDataComparisionGraph
Fetch line data comparision graph for provided filters




```swift
platformClient.analytics.fetchLineDataComparisionGraph(category: category, graphName: graphName, isStatRequired: isStatRequired, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| graphName | String | yes |  |   
| isStatRequired | Bool | yes |  |  
| body | DashboardFilterRequest | yes | Request body |


Fetch line data comparision graph for provided filters

*Returned Response:*




[[String: Any]](#[String: Any])

Fetch line graph data comparision for provided filters




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "x_axis": [
      "₹0- ₹500",
      "₹501- ₹1000",
      "₹1001- ₹1500",
      "₹1501- ₹2000",
      "₹2000+"
    ],
    "y_axis": {
      "data": [
        20009,
        10,
        2,
        8,
        25
      ]
    },
    "tooltip_text": [
      [
        "₹0- ₹500",
        "20009 Products"
      ],
      [
        "₹501- ₹1000",
        "10 Products"
      ],
      [
        "₹1001- ₹1500",
        "2 Products"
      ],
      [
        "₹1501- ₹2000",
        "8 Products"
      ],
      [
        "₹2000+",
        "25 Products"
      ]
    ],
    "stat_info": [
      {
        "id": "new_user_ratio",
        "actual_value": "0.5,",
        "change_in_value": -87.89,
        "trend_mapping": "red"
      }
    ]
  }
}
```
</details>

</details>









---


#### fetchProgressBarGraphData
Fetch progress bar graph for provided filters




```swift
platformClient.analytics.fetchProgressBarGraphData(category: category, graphName: graphName, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| graphName | String | yes |  |  
| body | DashboardFilterRequest | yes | Request body |


Fetch progress bar graph for provided filters

*Returned Response:*




[[String: Any]](#[String: Any])

Fetch progress bar graph for provided filters




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "x_axis": [
      "Clarks",
      "MUFTI",
      "celio",
      "CANTABIL",
      "Spykar"
    ],
    "y_axis": [
      {
        "max_value": 997734,
        "data": [
          {
            "id": "Clarks",
            "actual_value": 997734,
            "change_in_value": -35.55,
            "trend_mapping": "red"
          },
          {
            "id": "MUFTI",
            "actual_value": 970060,
            "change_in_value": -39.47,
            "trend_mapping": "red"
          },
          {
            "id": "celio",
            "actual_value": 811095,
            "change_in_value": -21.17,
            "trend_mapping": "red"
          },
          {
            "id": "CANTABIL",
            "actual_value": 654139,
            "change_in_value": -22.19,
            "trend_mapping": "red"
          },
          {
            "id": "Spykar",
            "actual_value": 492021,
            "change_in_value": 100,
            "trend_mapping": "green"
          }
        ]
      }
    ]
  }
}
```
</details>

</details>









---


#### fetchStatData
Fetch stat for provided filters




```swift
platformClient.analytics.fetchStatData(category: category, body: body) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |  
| body | DashboardFilterRequest | yes | Request body |


Fetch stat for provided filters

*Returned Response:*




[[String: Any]](#[String: Any])

Fetch stat for provided filters




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "x_axis": [
      "Clarks",
      "MUFTI",
      "celio",
      "CANTABIL",
      "Spykar"
    ],
    "y_axis": [
      {
        "max_value": 997734,
        "data": [
          {
            "id": "Clarks",
            "actual_value": 997734,
            "change_in_value": -35.55,
            "trend_mapping": "red"
          },
          {
            "id": "MUFTI",
            "actual_value": 970060,
            "change_in_value": -39.47,
            "trend_mapping": "red"
          },
          {
            "id": "celio",
            "actual_value": 811095,
            "change_in_value": -21.17,
            "trend_mapping": "red"
          },
          {
            "id": "CANTABIL",
            "actual_value": 654139,
            "change_in_value": -22.19,
            "trend_mapping": "red"
          },
          {
            "id": "Spykar",
            "actual_value": 492021,
            "change_in_value": 100,
            "trend_mapping": "green"
          }
        ]
      }
    ]
  }
}
```
</details>

</details>









---


#### cancelExportJob
Cancel export job




```swift
platformClient.analytics.cancelExportJob(category: category, fileName: fileName, graphName: graphName) { (response, error) in
    // Use response
}
```





| Argument | Type | Required | Description |
| -------- | ---- | -------- | ----------- | 
| category | String | yes |  |   
| fileName | String | yes |  |   
| graphName | String | yes |  |  



Cancel export job

*Returned Response:*




[[String: Any]](#[String: Any])

Cancel export job Response




<details>
<summary><i>&nbsp; Examples:</i></summary>


<details>
<summary><i>&nbsp; success</i></summary>

```json
{
  "value": {
    "result": "SUCCESS",
    "message": "Job cancelled successfully"
  }
}
```
</details>

</details>









---




### Schemas

 
 
 #### [SideBarResponse](#SideBarResponse)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | response | [Double]? |  yes  |  |

---


 
 
 #### [PermissionResponse](#PermissionResponse)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | response | [String]? |  yes  |  |

---


 
 
 #### [DropdownResponse](#DropdownResponse)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | text | String? |  yes  |  |
 | value | String? |  yes  |  |

---


 
 
 #### [DropdownRequest](#DropdownRequest)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | salesChannel | [String]? |  yes  |  |

---


 
 
 #### [SchedulerHistoryResponse](#SchedulerHistoryResponse)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | type | String? |  yes  |  |
 | key | String? |  yes  |  |
 | value | String? |  yes  |  |
 | createdOn | String? |  yes  |  |

---


 
 
 #### [SideBarRequest](#SideBarRequest)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | salesChannel | [String]? |  yes  |  |

---


 
 
 #### [ExportRequest](#ExportRequest)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | compareEndDate | String? |  yes  |  |
 | compareStartDate | String? |  yes  |  |
 | endDate | String? |  yes  |  |
 | orderPlatform | String? |  yes  |  |
 | orderStatus | String? |  yes  |  |
 | screenSize | String? |  yes  |  |
 | startDate | String? |  yes  |  |
 | storeCode | String? |  yes  |  |
 | valueType | String? |  yes  |  |

---


 
 
 #### [SaveTemplateResponse](#SaveTemplateResponse)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | seqNum | Double? |  yes  |  |
 | id | Double? |  yes  |  |
 | category | String? |  yes  |  |
 | reportType | String? |  yes  |  |
 | reportName | String? |  yes  |  |
 | company | String? |  yes  |  |
 | userName | String? |  yes  |  |
 | userId | String? |  yes  |  |
 | createdAt | String? |  yes  |  |
 | updatedAt | String? |  yes  |  |
 | config | [Config](#Config)? |  yes  |  |

---


 
 
 #### [Config](#Config)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | filters | [Filters](#Filters)? |  yes  |  |
 | selectedColumns | [[SelectedColumns](#SelectedColumns)]? |  yes  |  |

---


 
 
 #### [Style](#Style)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | maxLength | Int? |  yes  |  |

---


 
 
 #### [SelectedColumnsConfig](#SelectedColumnsConfig)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | sort | Bool? |  yes  |  |
 | style | [Style](#Style)? |  yes  |  |
 | format | Bool? |  yes  |  |
 | defaultColumn | Bool? |  yes  |  |
 | prefix | String? |  yes  |  |
 | suffix | String? |  yes  |  |

---


 
 
 #### [SelectedColumns](#SelectedColumns)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | config | [SelectedColumnsConfig](#SelectedColumnsConfig)? |  yes  |  |
 | displayName | String? |  yes  |  |
 | name | String? |  yes  |  |

---


 
 
 #### [SaveTemplateRequest](#SaveTemplateRequest)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | category | String? |  yes  |  |
 | reportType | String? |  yes  |  |
 | reportName | String? |  yes  |  |
 | config | [Config](#Config)? |  yes  |  |

---


 
 
 #### [LayoutString](#LayoutString)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | dataSource | String? |  yes  |  |

---


 
 
 #### [Dlayout](#Dlayout)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | dataSource | String? |  yes  |  |
 | orders | [LayoutString](#LayoutString)? |  yes  |  |
 | inventory | [LayoutString](#LayoutString)? |  yes  |  |
 | logistics | [LayoutString](#LayoutString)? |  yes  |  |
 | finance | [LayoutString](#LayoutString)? |  yes  |  |
 | users | [LayoutString](#LayoutString)? |  yes  |  |

---


 
 
 #### [Range](#Range)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | text | String? |  yes  |  |
 | value | String? |  yes  |  |
 | minDuration | Int? |  yes  |  |
 | maxDuration | Int? |  yes  |  |

---


 
 
 #### [LayoutGlobalFilters](#LayoutGlobalFilters)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | String? |  yes  |  |
 | name | String? |  yes  |  |
 | placeholder | String? |  yes  |  |
 | searchable | Bool? |  yes  |  |
 | showCollapse | Bool? |  yes  |  |
 | showTags | Bool? |  yes  |  |
 | showClear | Bool? |  yes  |  |
 | isDefaultRequired | Bool? |  yes  |  |
 | dataSource | Bool? |  yes  |  |
 | selectAllEnabled | Bool? |  yes  |  |
 | defaultAllSelected | Bool? |  yes  |  |
 | isMultiselect | Bool? |  yes  |  |
 | reset | Bool? |  yes  |  |
 | showCompare | Bool? |  yes  |  |
 | allowedShortcuts | [String]? |  yes  |  |
 | type | String? |  yes  |  |
 | range | [[Range](#Range)]? |  yes  |  |

---


 
 
 #### [LayoutConfig](#LayoutConfig)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | layoutDetails | [Dlayout](#Dlayout)? |  yes  |  |
 | globalFilters | [[LayoutGlobalFilters](#LayoutGlobalFilters)]? |  yes  |  |

---


 
 
 #### [LayoutResponseModel](#LayoutResponseModel)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | Int? |  yes  |  |
 | company | String? |  yes  |  |
 | userId | String? |  yes  |  |
 | layoutConfig | [LayoutConfig](#LayoutConfig)? |  yes  |  |
 | layoutName | String? |  yes  |  |
 | isDefault | Bool? |  yes  |  |

---


 
 
 #### [Page](#Page)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | current | Int? |  yes  |  |
 | size | Int? |  yes  |  |
 | itemTotal | Int? |  yes  |  |
 | totalPage | Int? |  yes  |  |
 | hasNext | Bool? |  yes  |  |
 | hasPrevious | Bool? |  yes  |  |

---


 
 
 #### [DashboardFilterRequest](#DashboardFilterRequest)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | screenSize | String? |  yes  |  |
 | filters | [Filters](#Filters)? |  yes  |  |

---


 
 
 #### [ReportFiltersRequest](#ReportFiltersRequest)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | screenSize | String? |  yes  |  |
 | page | [Page](#Page)? |  yes  |  |
 | sort | [[Sort](#Sort)]? |  yes  |  |
 | columns | [[Columns](#Columns)]? |  yes  |  |
 | filters | [Filters](#Filters)? |  yes  |  |

---


 
 
 #### [Columns](#Columns)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | config | [SelectedColumnsConfig](#SelectedColumnsConfig)? |  yes  |  |
 | displayName | String? |  yes  |  |
 | name | String? |  yes  |  |
 | sortName | String? |  yes  |  |

---


 
 
 #### [Sort](#Sort)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | columnName | String? |  yes  |  |
 | order | String? |  yes  |  |

---


 
 
 #### [StatFiltersItems](#StatFiltersItems)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | String? |  yes  |  |
 | actualValue | Double? |  yes  |  |
 | changeInValue | Double? |  yes  |  |
 | trendMapping | String? |  yes  |  |

---


 
 
 #### [StatFiltersResponse](#StatFiltersResponse)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | items | [[StatFiltersItems](#StatFiltersItems)]? |  yes  |  |

---


 
 
 #### [StatFiltersRequest](#StatFiltersRequest)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | filters | [Filters](#Filters)? |  yes  |  |

---


 
 
 #### [GlobalFilters](#GlobalFilters)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | String? |  yes  |  |
 | name | String? |  yes  |  |
 | placeholder | String? |  yes  |  |
 | searchable | Bool? |  yes  |  |
 | showCollapse | Bool? |  yes  |  |
 | showTags | Bool? |  yes  |  |
 | showClear | Bool? |  yes  |  |
 | isDefaultRequired | Bool? |  yes  |  |
 | dataSource | Bool? |  yes  |  |
 | showCompare | Bool? |  yes  |  |
 | allowedShortcuts | [String]? |  yes  |  |
 | selectAllEnabled | Bool? |  yes  |  |
 | defaultAllSelected | Bool? |  yes  |  |
 | isMultiselect | Bool? |  yes  |  |
 | reset | Bool? |  yes  |  |
 | endDate | String? |  yes  |  |
 | startDate | String? |  yes  |  |
 | shortcut | String? |  yes  |  |
 | salesChannel | [String]? |  yes  |  |
 | timeFrameClause | String? |  yes  |  |
 | compareStartDate | String? |  yes  |  |
 | compareEndDate | String? |  yes  |  |
 | isHidden | Bool? |  yes  |  |
 | type | String? |  yes  |  |
 | range | [[Range](#Range)]? |  yes  |  |

---


 
 
 #### [ConditionalFilters](#ConditionalFilters)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | blocks | [[Block](#Block)]? |  yes  |  |
 | logicalOperator | String? |  yes  |  |
 | valid | Bool? |  yes  |  |

---


 
 
 #### [Block](#Block)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | columnName | String? |  yes  |  |
 | conditionalOperator | String? |  yes  |  |
 | value | String? |  yes  |  |
 | type | String? |  yes  |  |
 | logicalOperator | String? |  yes  |  |
 | valid | Bool? |  yes  |  |

---


 
 
 #### [ExecutionDetails](#ExecutionDetails)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | lastRunAt | Int? |  yes  |  |
 | nextRunAt | Int? |  yes  |  |

---


 
 
 #### [EmailDetails](#EmailDetails)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | Int? |  yes  |  |
 | to | [String]? |  yes  |  |
 | cc | [String]? |  yes  |  |
 | bcc | [String]? |  yes  |  |
 | subject | String? |  yes  |  |
 | body | String? |  yes  |  |
 | attachmentType | String? |  yes  |  |
 | reportTemplatesIds | [Int]? |  yes  |  |
 | reportTemplates | [[ReportTemplate](#ReportTemplate)]? |  yes  |  |

---


 
 
 #### [ReportTemplate](#ReportTemplate)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | Int? |  yes  |  |
 | seqNum | Int? |  yes  |  |
 | reportName | String? |  yes  |  |
 | config | [Config](#Config)? |  yes  |  |
 | reportType | String? |  yes  |  |
 | category | String? |  yes  |  |

---


 
 
 #### [SchedulerDetails](#SchedulerDetails)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | Int? |  yes  |  |
 | status | String? |  yes  |  |
 | schedulerId | String? |  yes  |  |
 | companyId | String? |  yes  |  |
 | createdAt | String? |  yes  |  |
 | updatedAt | String? |  yes  |  |
 | cronExpression | String? |  yes  |  |
 | destinationType | String? |  yes  |  |
 | destinationId | Int? |  yes  |  |
 | frequency | String? |  yes  |  |
 | config | [String: Any]? |  yes  |  |
 | emailDetail | [EmailDetails](#EmailDetails)? |  yes  |  |

---


 
 
 #### [TemplateResponseModel](#TemplateResponseModel)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | executionDetails | [ExecutionDetails](#ExecutionDetails)? |  yes  |  |
 | schedulerDetails | [SchedulerDetails](#SchedulerDetails)? |  yes  |  |

---


 
 
 #### [SchedulerResponseBody](#SchedulerResponseBody)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | message | String? |  yes  |  |

---


 
 
 #### [ScheduleRequestBody](#ScheduleRequestBody)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | status | String? |  yes  |  |
 | emailDetails | [EmailDetails](#EmailDetails)? |  yes  |  |
 | freqDetails | [String: Any]? |  yes  |  |

---


 
 
 #### [TableRequestBody](#TableRequestBody)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | screenSize | String? |  yes  |  |
 | page | [Page](#Page)? |  yes  |  |
 | sort | [[Sort](#Sort)]? |  yes  |  |
 | columns | [[Columns](#Columns)]? |  yes  |  |
 | filters | [Filters](#Filters)? |  yes  |  |

---


 
 
 #### [Filters](#Filters)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | graphFilter | [String]? |  yes  |  |
 | screenSize | Double? |  yes  |  |
 | reportFilters | [[ReportFilters](#ReportFilters)]? |  yes  |  |
 | exportFilters | [String]? |  yes  |  |
 | globalFilters | [GlobalFilters](#GlobalFilters)? |  yes  |  |
 | sidebarFilters | [[String: Any]]? |  yes  |  |
 | conditionalFilters | [[ConditionalFilters](#ConditionalFilters)]? |  yes  |  |
 | selectedColumns | [[SelectedColumns](#SelectedColumns)]? |  yes  |  |

---


 
 
 #### [TableResponseModel](#TableResponseModel)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | rows | [[String: Any]]? |  yes  |  |
 | page | [Page](#Page)? |  yes  |  |

---


 
 
 #### [ReportConfig](#ReportConfig)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | capitalize | Bool? |  yes  |  |
 | enableSelectAll | Bool? |  yes  |  |
 | sort | Bool? |  yes  |  |
 | type | String? |  yes  |  |

---


 
 
 #### [StringItem](#StringItem)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | Int? |  yes  |  |

---


 
 
 #### [BooleanItem](#BooleanItem)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | Bool? |  yes  |  |

---


 
 
 #### [IntegerItem](#IntegerItem)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | String? |  yes  |  |

---


 
 
 #### [Action](#Action)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | icons | [[Icon](#Icon)]? |  yes  |  |

---


 
 
 #### [Icon](#Icon)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | name | String? |  yes  |  |
 | action | String? |  yes  |  |

---


 
 
 #### [Category](#Category)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | type | String? |  yes  |  |
 | value | String? |  yes  |  |

---


 
 
 #### [ReportItemConfig](#ReportItemConfig)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | [ReportConfig](#ReportConfig)? |  yes  |  |

---


 
 
 #### [ValueNumber](#ValueNumber)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | Double? |  yes  |  |

---


 
 
 #### [ValueString](#ValueString)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | String? |  yes  |  |

---


 
 
 #### [ValueCategory](#ValueCategory)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | type | String? |  yes  |  |
 | value | String? |  yes  |  |

---


 
 
 #### [ActionObject](#ActionObject)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | String? |  yes  |  |
 | text | String? |  yes  |  |

---


 
 
 #### [ValueStatus](#ValueStatus)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | type | String? |  yes  |  |
 | value | String? |  yes  |  |
 | icon | String? |  yes  |  |
 | action | [[ActionObject](#ActionObject)]? |  yes  |  |

---


 
 
 #### [ValueTo](#ValueTo)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | [String]? |  yes  |  |

---


 
 
 #### [Tooltip](#Tooltip)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | message | String? |  yes  |  |

---


 
 
 #### [Frequency](#Frequency)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | value | String? |  yes  |  |
 | icon | String? |  yes  |  |
 | tooltip | [Tooltip](#Tooltip)? |  yes  |  |

---


 
 
 #### [ReportItemValue](#ReportItemValue)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | [ValueNumber](#ValueNumber)? |  yes  |  |
 | name | [ValueString](#ValueString)? |  yes  |  |
 | subject | [ValueString](#ValueString)? |  yes  |  |
 | category | [ValueCategory](#ValueCategory)? |  yes  |  |
 | createdAt | [ValueString](#ValueString)? |  yes  |  |
 | status | [ValueStatus](#ValueStatus)? |  yes  |  |
 | to | [ValueTo](#ValueTo)? |  yes  |  |
 | executionDetails | [ExecutionDetails](#ExecutionDetails)? |  yes  |  |
 | frequency | [Frequency](#Frequency)? |  yes  |  |

---


 
 
 #### [ReportItem](#ReportItem)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | [IntegerItem](#IntegerItem)? |  yes  |  |
 | name | [StringItem](#StringItem)? |  yes  |  |
 | seqNum | [IntegerItem](#IntegerItem)? |  yes  |  |
 | category | [Category](#Category)? |  yes  |  |
 | lastViewed | [StringItem](#StringItem)? |  yes  |  |
 | userName | [StringItem](#StringItem)? |  yes  |  |
 | config | [ReportItemConfig](#ReportItemConfig)? |  yes  |  |
 | type | [StringItem](#StringItem)? |  yes  |  |
 | isDefault | [BooleanItem](#BooleanItem)? |  yes  |  |
 | action | [Action](#Action)? |  yes  |  |
 | values | [ReportItemValue](#ReportItemValue)? |  yes  |  |

---


 
 
 #### [SchedulerDetailsRespModel](#SchedulerDetailsRespModel)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | headers | [[Headers](#Headers)]? |  yes  |  |
 | records | [[ReportItem](#ReportItem)]? |  yes  |  |

---


 
 
 #### [Headers](#Headers)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | config | [ReportConfig](#ReportConfig)? |  yes  |  |
 | displayName | String? |  yes  |  |
 | name | String? |  yes  |  |

---


 
 
 #### [ReportsDetailsRespModel](#ReportsDetailsRespModel)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | headers | [[Headers](#Headers)]? |  yes  |  |
 | records | [[ReportItem](#ReportItem)]? |  yes  |  |
 | page | [Page](#Page)? |  yes  |  |

---


 
 
 #### [ReportFilters](#ReportFilters)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | String? |  yes  |  |
 | name | String? |  yes  |  |
 | placeholder | String? |  yes  |  |
 | searchable | Bool? |  yes  |  |
 | showCollapse | Bool? |  yes  |  |
 | showTags | Bool? |  yes  |  |
 | showClear | Bool? |  yes  |  |
 | isDefaultRequired | Bool? |  yes  |  |
 | dataSource | Bool? |  yes  |  |
 | selectAllEnabled | Bool? |  yes  |  |
 | defaultAllSelected | Bool? |  yes  |  |
 | isMultiselect | Bool? |  yes  |  |
 | reset | Bool? |  yes  |  |

---


 
 
 #### [TableInfo](#TableInfo)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | datasource | String? |  yes  |  |
 | columns | [[Columns](#Columns)]? |  yes  |  |
 | selectedColumns | [[SelectedColumns](#SelectedColumns)]? |  yes  |  |

---


 
 
 #### [Row](#Row)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | columnName | String? |  yes  |  |
 | type | String? |  yes  |  |
 | datasource | String? |  yes  |  |
 | name | String? |  yes  |  |

---


 
 
 #### [ConditionalFilterData](#ConditionalFilterData)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | rows | [[Row](#Row)]? |  yes  |  |
 | limit | Int? |  yes  |  |

---


 
 
 #### [LayoutConfigDetails](#LayoutConfigDetails)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | category | String? |  yes  |  |
 | displayName | String? |  yes  |  |
 | reportName | String? |  yes  |  |
 | id | String? |  yes  |  |
 | tableInfo | [TableInfo](#TableInfo)? |  yes  |  |
 | sidebarFilters | [[String: Any]]? |  yes  |  |
 | conditionalFiltersData | [ConditionalFilterData](#ConditionalFilterData)? |  yes  |  |
 | reportFilters | [[ReportFilters](#ReportFilters)]? |  yes  |  |
 | globalFilters | [[GlobalFilters](#GlobalFilters)]? |  yes  |  |
 | layoutName | String? |  yes  |  |
 | isDefault | Bool? |  yes  |  |
 | showTooltip | Bool? |  yes  |  |
 | tooltipText | String? |  yes  |  |

---


 
 
 #### [LayoutDetailConfig](#LayoutDetailConfig)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | reportFilters | [[ReportFilters](#ReportFilters)]? |  yes  |  |
 | exportFilters | [String]? |  yes  |  |
 | selectedColumns | [[SelectedColumns](#SelectedColumns)]? |  yes  |  |

---


 
 
 #### [Layout](#Layout)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | Int? |  yes  |  |
 | company | String? |  yes  |  |
 | userId | String? |  yes  |  |
 | layoutName | String? |  yes  |  |
 | isDefault | Bool? |  yes  |  |
 | layoutConfig | [LayoutConfigDetails](#LayoutConfigDetails)? |  yes  |  |
 | config | [LayoutDetailConfig](#LayoutDetailConfig)? |  yes  |  |

---


 
 
 #### [TemplateLayoutRespModel](#TemplateLayoutRespModel)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | id | Int? |  yes  | Unique identifier for the page view |
 | name | String? |  yes  | Name of the page view |
 | reportType | String? |  yes  | Type of the report for the page view |
 | category | String? |  yes  | Category of the page view |
 | isEditable | Bool? |  yes  | Indicates if the page view is editable |
 | layout | [Layout](#Layout)? |  yes  |  |
 | config | [Config](#Config)? |  yes  |  |

---


 
 
 #### [FilterRespBody](#FilterRespBody)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | text | String? |  yes  |  |
 | value | String? |  yes  |  |

---


 
 
 #### [TemplateDetailsRequestBody](#TemplateDetailsRequestBody)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | filters | [[GraphFilters](#GraphFilters)]? |  yes  |  |
 | sortBy | [[Sort](#Sort)]? |  yes  |  |
 | page | [Page](#Page)? |  yes  |  |

---


 
 
 #### [GraphFilters](#GraphFilters)

 | Properties | Type | Nullable | Description |
 | ---------- | ---- | -------- | ----------- |
 | columnName | String? |  yes  |  |
 | conditionalOperator | String? |  yes  |  |
 | value | String? |  yes  |  |
 | type | String? |  yes  |  |

---




### Enums





 #### [PageType](#PageType)
 Type : string

 | Name | Value | Description |
 | ---- | ----- | ----------- |
 | aboutUs | about-us | Symbolic link for About Us: /about-us |
 | addresses | addresses | Symbolic link for Saved Addresses: /profile/address |
 | blog | blog | Symbolic link for Blog: /blog/:slug |
 | brands | brands | Symbolic link for Brands: /brands/:department |
 | cards | cards | Symbolic link for Saved Cards: /profile/my-cards |
 | cart | cart | Symbolic link for Cart: /cart/bag/ |
 | categories | categories | Symbolic link for Categories: /categories/:department |
 | brand | brand | Symbolic link for Brand: /brand/:slug |
 | category | category | Symbolic link for Category: /category/:slug |
 | collection | collection | Symbolic link for Collection: /collection/:slug |
 | collections | collections | Symbolic link for Collections: /collections/ |
 | contactUs | contact-us | Symbolic link for Contact Us: /contact-us/ |
 | externalLink | external | Symbolic link for External Link: /external/ |
 | faq | faq | Symbolic link for FAQ: /faq |
 | freshchat | freshchat | Symbolic link for Chat by Freshchat: /freshchat |
 | home | home | Symbolic link for Home: / |
 | notificationSettings | notification-settings | Symbolic link for Notification Settings: /notification-settings |
 | orders | orders | Symbolic link for Orders: /profile/orders |
 | page | page | Symbolic link for Page: /page/:slug |
 | policy | policy | Symbolic link for Privacy Policy: /privacy-policy |
 | product | product | Symbolic link for Product: /product/:slug |
 | productRequest | product-request | Symbolic link for Product Request: /product-request/ |
 | products | products | Symbolic link for Products: /products/ |
 | profile | profile | Symbolic link for Profile: /profile |
 | profileOrderShipment | profile-order-shipment | Symbolic link for profile orders shipment: /profile/orders/shipment/:shipmentid |
 | profileBasic | profile-basic | Symbolic link for Basic Profile: /profile/details |
 | profileCompany | profile-company | Symbolic link for Profile Company: /profile/company |
 | profileEmails | profile-emails | Symbolic link for Profile Emails: /profile/email |
 | profilePhones | profile-phones | Symbolic link for Profile Phones: /profile/phone |
 | rateUs | rate-us | Symbolic link for Rate Us: /rate-us |
 | referEarn | refer-earn | Symbolic link for Refer & Earn: /profile/refer-earn |
 | settings | settings | Symbolic link for Settings: /setting/currency |
 | sharedCart | shared-cart | Symbolic link for Shared Cart: /shared-cart/:token |
 | tnc | tnc | Symbolic link for Terms and Conditions: /terms-and-conditions |
 | trackOrder | track-order | Symbolic link for Track Order: /order-tracking/:orderId |
 | wishlist | wishlist | Symbolic link for Wishlist: /wishlist/ |
 | sections | sections | Symbolic link for Sections: /sections/:group |
 | form | form | Symbolic link for Form: /form/:slug |
 | cartDelivery | cart-delivery | Symbolic link for Cart Delivery: /cart/delivery |
 | cartPayment | cart-payment | Symbolic link for Cart Payment Information: /cart/payment-info |
 | cartReview | cart-review | Symbolic link for Cart Order Review: /cart/order-review |
 | login | login | Symbolic link for Login: /auth/login |
 | register | register | Symbolic link for Register: /auth/register |
 | shippingPolicy | shipping-policy | Symbolic link for Shipping policy: /shipping-policy |
 | returnPolicy | return-policy | Symbolic link for Return policy: /return-policy |
 | orderStatus | order-status | Symbolic link for Order status: /cart/order-status |

---





