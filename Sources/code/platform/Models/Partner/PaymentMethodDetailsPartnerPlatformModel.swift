

import Foundation


public extension PlatformClient.Partner {
    /*
        Model: PaymentMethodDetails
        Used By: Partner
    */

    class PaymentMethodDetails: Codable {
        
        
        public var slug: String?
        
        public var name: String?
        
        public var id: Double?
        
        public var transactionFee: Double?
        

        public enum CodingKeys: String, CodingKey {
            
            case slug = "slug"
            
            case name = "name"
            
            case id = "id"
            
            case transactionFee = "transaction_fee"
            
        }

        public init(id: Double? = nil, name: String? = nil, slug: String? = nil, transactionFee: Double? = nil) {
            
            self.slug = slug
            
            self.name = name
            
            self.id = id
            
            self.transactionFee = transactionFee
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    slug = try container.decode(String.self, forKey: .slug)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(String.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    id = try container.decode(Double.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    transactionFee = try container.decode(Double.self, forKey: .transactionFee)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(slug, forKey: .slug)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(transactionFee, forKey: .transactionFee)
            
            
        }
        
    }
}



public extension PlatformClient.ApplicationClient.Partner {
    /*
        Model: PaymentMethodDetails
        Used By: Partner
    */

    class PaymentMethodDetails: Codable {
        
        
        public var slug: String?
        
        public var name: String?
        
        public var id: Double?
        
        public var transactionFee: Double?
        

        public enum CodingKeys: String, CodingKey {
            
            case slug = "slug"
            
            case name = "name"
            
            case id = "id"
            
            case transactionFee = "transaction_fee"
            
        }

        public init(id: Double? = nil, name: String? = nil, slug: String? = nil, transactionFee: Double? = nil) {
            
            self.slug = slug
            
            self.name = name
            
            self.id = id
            
            self.transactionFee = transactionFee
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    slug = try container.decode(String.self, forKey: .slug)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(String.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    id = try container.decode(Double.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    transactionFee = try container.decode(Double.self, forKey: .transactionFee)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(slug, forKey: .slug)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(transactionFee, forKey: .transactionFee)
            
            
        }
        
    }
}


