

import Foundation


public extension PlatformClient.Partner {
    /*
        Model: LaunchTypeConfig
        Used By: Partner
    */

    class LaunchTypeConfig: Codable {
        
        
        public var paymentMethods: [PaymentMethodDetails]?
        
        public var apiVersion: String?
        
        public var country: [String]?
        
        public var currency: [String]?
        
        public var partnershipMode: Bool?
        
        public var transactionFeeType: String?
        
        public var autoCapture: Bool?
        
        public var checkoutType: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case paymentMethods = "payment_methods"
            
            case apiVersion = "api_version"
            
            case country = "country"
            
            case currency = "currency"
            
            case partnershipMode = "partnership_mode"
            
            case transactionFeeType = "transaction_fee_type"
            
            case autoCapture = "auto_capture"
            
            case checkoutType = "checkout_type"
            
        }

        public init(apiVersion: String? = nil, autoCapture: Bool? = nil, checkoutType: String? = nil, country: [String]? = nil, currency: [String]? = nil, partnershipMode: Bool? = nil, paymentMethods: [PaymentMethodDetails]? = nil, transactionFeeType: String? = nil) {
            
            self.paymentMethods = paymentMethods
            
            self.apiVersion = apiVersion
            
            self.country = country
            
            self.currency = currency
            
            self.partnershipMode = partnershipMode
            
            self.transactionFeeType = transactionFeeType
            
            self.autoCapture = autoCapture
            
            self.checkoutType = checkoutType
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    paymentMethods = try container.decode([PaymentMethodDetails].self, forKey: .paymentMethods)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    apiVersion = try container.decode(String.self, forKey: .apiVersion)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    country = try container.decode([String].self, forKey: .country)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    currency = try container.decode([String].self, forKey: .currency)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    partnershipMode = try container.decode(Bool.self, forKey: .partnershipMode)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    transactionFeeType = try container.decode(String.self, forKey: .transactionFeeType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    autoCapture = try container.decode(Bool.self, forKey: .autoCapture)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    checkoutType = try container.decode(String.self, forKey: .checkoutType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(paymentMethods, forKey: .paymentMethods)
            
            
            
            
            try? container.encodeIfPresent(apiVersion, forKey: .apiVersion)
            
            
            
            
            try? container.encodeIfPresent(country, forKey: .country)
            
            
            
            
            try? container.encodeIfPresent(currency, forKey: .currency)
            
            
            
            
            try? container.encodeIfPresent(partnershipMode, forKey: .partnershipMode)
            
            
            
            
            try? container.encodeIfPresent(transactionFeeType, forKey: .transactionFeeType)
            
            
            
            
            try? container.encodeIfPresent(autoCapture, forKey: .autoCapture)
            
            
            
            
            try? container.encodeIfPresent(checkoutType, forKey: .checkoutType)
            
            
        }
        
    }
}



public extension PlatformClient.ApplicationClient.Partner {
    /*
        Model: LaunchTypeConfig
        Used By: Partner
    */

    class LaunchTypeConfig: Codable {
        
        
        public var paymentMethods: [PaymentMethodDetails]?
        
        public var apiVersion: String?
        
        public var country: [String]?
        
        public var currency: [String]?
        
        public var partnershipMode: Bool?
        
        public var transactionFeeType: String?
        
        public var autoCapture: Bool?
        
        public var checkoutType: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case paymentMethods = "payment_methods"
            
            case apiVersion = "api_version"
            
            case country = "country"
            
            case currency = "currency"
            
            case partnershipMode = "partnership_mode"
            
            case transactionFeeType = "transaction_fee_type"
            
            case autoCapture = "auto_capture"
            
            case checkoutType = "checkout_type"
            
        }

        public init(apiVersion: String? = nil, autoCapture: Bool? = nil, checkoutType: String? = nil, country: [String]? = nil, currency: [String]? = nil, partnershipMode: Bool? = nil, paymentMethods: [PaymentMethodDetails]? = nil, transactionFeeType: String? = nil) {
            
            self.paymentMethods = paymentMethods
            
            self.apiVersion = apiVersion
            
            self.country = country
            
            self.currency = currency
            
            self.partnershipMode = partnershipMode
            
            self.transactionFeeType = transactionFeeType
            
            self.autoCapture = autoCapture
            
            self.checkoutType = checkoutType
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    paymentMethods = try container.decode([PaymentMethodDetails].self, forKey: .paymentMethods)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    apiVersion = try container.decode(String.self, forKey: .apiVersion)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    country = try container.decode([String].self, forKey: .country)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    currency = try container.decode([String].self, forKey: .currency)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    partnershipMode = try container.decode(Bool.self, forKey: .partnershipMode)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    transactionFeeType = try container.decode(String.self, forKey: .transactionFeeType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    autoCapture = try container.decode(Bool.self, forKey: .autoCapture)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    checkoutType = try container.decode(String.self, forKey: .checkoutType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(paymentMethods, forKey: .paymentMethods)
            
            
            
            
            try? container.encodeIfPresent(apiVersion, forKey: .apiVersion)
            
            
            
            
            try? container.encodeIfPresent(country, forKey: .country)
            
            
            
            
            try? container.encodeIfPresent(currency, forKey: .currency)
            
            
            
            
            try? container.encodeIfPresent(partnershipMode, forKey: .partnershipMode)
            
            
            
            
            try? container.encodeIfPresent(transactionFeeType, forKey: .transactionFeeType)
            
            
            
            
            try? container.encodeIfPresent(autoCapture, forKey: .autoCapture)
            
            
            
            
            try? container.encodeIfPresent(checkoutType, forKey: .checkoutType)
            
            
        }
        
    }
}


