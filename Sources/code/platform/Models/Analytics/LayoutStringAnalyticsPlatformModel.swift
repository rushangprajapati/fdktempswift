

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: LayoutString
        Used By: Analytics
    */

    class LayoutString: Codable {
        
        
        public var dataSource: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case dataSource = "data_source"
            
        }

        public init(dataSource: String? = nil) {
            
            self.dataSource = dataSource
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    dataSource = try container.decode(String.self, forKey: .dataSource)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(dataSource, forKey: .dataSource)
            
            
        }
        
    }
}




