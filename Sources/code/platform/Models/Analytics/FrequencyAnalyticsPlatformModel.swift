

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Frequency
        Used By: Analytics
    */

    class Frequency: Codable {
        
        
        public var value: String?
        
        public var icon: String?
        
        public var tooltip: Tooltip?
        

        public enum CodingKeys: String, CodingKey {
            
            case value = "value"
            
            case icon = "icon"
            
            case tooltip = "tooltip"
            
        }

        public init(icon: String? = nil, tooltip: Tooltip? = nil, value: String? = nil) {
            
            self.value = value
            
            self.icon = icon
            
            self.tooltip = tooltip
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    value = try container.decode(String.self, forKey: .value)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    icon = try container.decode(String.self, forKey: .icon)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    tooltip = try container.decode(Tooltip.self, forKey: .tooltip)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(value, forKey: .value)
            
            
            
            
            try? container.encodeIfPresent(icon, forKey: .icon)
            
            
            
            
            try? container.encodeIfPresent(tooltip, forKey: .tooltip)
            
            
        }
        
    }
}




