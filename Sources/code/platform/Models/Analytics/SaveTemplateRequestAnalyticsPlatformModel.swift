

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: SaveTemplateRequest
        Used By: Analytics
    */

    class SaveTemplateRequest: Codable {
        
        
        public var category: String?
        
        public var reportType: String?
        
        public var reportName: String?
        
        public var config: Config?
        

        public enum CodingKeys: String, CodingKey {
            
            case category = "category"
            
            case reportType = "report_type"
            
            case reportName = "report_name"
            
            case config = "config"
            
        }

        public init(category: String? = nil, config: Config? = nil, reportName: String? = nil, reportType: String? = nil) {
            
            self.category = category
            
            self.reportType = reportType
            
            self.reportName = reportName
            
            self.config = config
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    category = try container.decode(String.self, forKey: .category)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportType = try container.decode(String.self, forKey: .reportType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportName = try container.decode(String.self, forKey: .reportName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    config = try container.decode(Config.self, forKey: .config)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(category, forKey: .category)
            
            
            
            
            try? container.encodeIfPresent(reportType, forKey: .reportType)
            
            
            
            
            try? container.encodeIfPresent(reportName, forKey: .reportName)
            
            
            
            
            try? container.encodeIfPresent(config, forKey: .config)
            
            
        }
        
    }
}




