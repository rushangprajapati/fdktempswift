

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ReportConfig
        Used By: Analytics
    */

    class ReportConfig: Codable {
        
        
        public var capitalize: Bool?
        
        public var enableSelectAll: Bool?
        
        public var sort: Bool?
        
        public var type: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case capitalize = "capitalize"
            
            case enableSelectAll = "enable_select_all"
            
            case sort = "sort"
            
            case type = "type"
            
        }

        public init(capitalize: Bool? = nil, enableSelectAll: Bool? = nil, sort: Bool? = nil, type: String? = nil) {
            
            self.capitalize = capitalize
            
            self.enableSelectAll = enableSelectAll
            
            self.sort = sort
            
            self.type = type
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    capitalize = try container.decode(Bool.self, forKey: .capitalize)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    enableSelectAll = try container.decode(Bool.self, forKey: .enableSelectAll)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    sort = try container.decode(Bool.self, forKey: .sort)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    type = try container.decode(String.self, forKey: .type)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(capitalize, forKey: .capitalize)
            
            
            
            
            try? container.encodeIfPresent(enableSelectAll, forKey: .enableSelectAll)
            
            
            
            
            try? container.encodeIfPresent(sort, forKey: .sort)
            
            
            
            
            try? container.encodeIfPresent(type, forKey: .type)
            
            
        }
        
    }
}




