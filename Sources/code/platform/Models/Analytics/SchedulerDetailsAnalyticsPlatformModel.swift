

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: SchedulerDetails
        Used By: Analytics
    */

    class SchedulerDetails: Codable {
        
        
        public var id: Int?
        
        public var status: String?
        
        public var schedulerId: String?
        
        public var companyId: String?
        
        public var createdAt: String?
        
        public var updatedAt: String?
        
        public var cronExpression: String?
        
        public var destinationType: String?
        
        public var destinationId: Int?
        
        public var frequency: String?
        
        public var config: [String: Any]?
        
        public var emailDetail: EmailDetails?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case status = "status"
            
            case schedulerId = "scheduler_id"
            
            case companyId = "company_id"
            
            case createdAt = "created_at"
            
            case updatedAt = "updated_at"
            
            case cronExpression = "cron_expression"
            
            case destinationType = "destination_type"
            
            case destinationId = "destination_id"
            
            case frequency = "frequency"
            
            case config = "config"
            
            case emailDetail = "email_detail"
            
        }

        public init(companyId: String? = nil, config: [String: Any]? = nil, createdAt: String? = nil, cronExpression: String? = nil, destinationId: Int? = nil, destinationType: String? = nil, emailDetail: EmailDetails? = nil, frequency: String? = nil, id: Int? = nil, schedulerId: String? = nil, status: String? = nil, updatedAt: String? = nil) {
            
            self.id = id
            
            self.status = status
            
            self.schedulerId = schedulerId
            
            self.companyId = companyId
            
            self.createdAt = createdAt
            
            self.updatedAt = updatedAt
            
            self.cronExpression = cronExpression
            
            self.destinationType = destinationType
            
            self.destinationId = destinationId
            
            self.frequency = frequency
            
            self.config = config
            
            self.emailDetail = emailDetail
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(Int.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    status = try container.decode(String.self, forKey: .status)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    schedulerId = try container.decode(String.self, forKey: .schedulerId)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    companyId = try container.decode(String.self, forKey: .companyId)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    createdAt = try container.decode(String.self, forKey: .createdAt)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    updatedAt = try container.decode(String.self, forKey: .updatedAt)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    cronExpression = try container.decode(String.self, forKey: .cronExpression)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    destinationType = try container.decode(String.self, forKey: .destinationType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    destinationId = try container.decode(Int.self, forKey: .destinationId)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    frequency = try container.decode(String.self, forKey: .frequency)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    config = try container.decode([String: Any].self, forKey: .config)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    emailDetail = try container.decode(EmailDetails.self, forKey: .emailDetail)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(status, forKey: .status)
            
            
            
            
            try? container.encodeIfPresent(schedulerId, forKey: .schedulerId)
            
            
            
            
            try? container.encodeIfPresent(companyId, forKey: .companyId)
            
            
            
            
            try? container.encodeIfPresent(createdAt, forKey: .createdAt)
            
            
            
            
            try? container.encodeIfPresent(updatedAt, forKey: .updatedAt)
            
            
            
            
            try? container.encodeIfPresent(cronExpression, forKey: .cronExpression)
            
            
            
            
            try? container.encodeIfPresent(destinationType, forKey: .destinationType)
            
            
            
            
            try? container.encodeIfPresent(destinationId, forKey: .destinationId)
            
            
            
            
            try? container.encodeIfPresent(frequency, forKey: .frequency)
            
            
            
            
            try? container.encodeIfPresent(config, forKey: .config)
            
            
            
            
            try? container.encodeIfPresent(emailDetail, forKey: .emailDetail)
            
            
        }
        
    }
}




