

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ScheduleRequestBody
        Used By: Analytics
    */

    class ScheduleRequestBody: Codable {
        
        
        public var status: String?
        
        public var emailDetails: EmailDetails?
        
        public var freqDetails: [String: Any]?
        

        public enum CodingKeys: String, CodingKey {
            
            case status = "status"
            
            case emailDetails = "email_details"
            
            case freqDetails = "freq_details"
            
        }

        public init(emailDetails: EmailDetails? = nil, freqDetails: [String: Any]? = nil, status: String? = nil) {
            
            self.status = status
            
            self.emailDetails = emailDetails
            
            self.freqDetails = freqDetails
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    status = try container.decode(String.self, forKey: .status)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    emailDetails = try container.decode(EmailDetails.self, forKey: .emailDetails)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    freqDetails = try container.decode([String: Any].self, forKey: .freqDetails)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(status, forKey: .status)
            
            
            
            
            try? container.encodeIfPresent(emailDetails, forKey: .emailDetails)
            
            
            
            
            try? container.encodeIfPresent(freqDetails, forKey: .freqDetails)
            
            
        }
        
    }
}




