

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Layout
        Used By: Analytics
    */

    class Layout: Codable {
        
        
        public var id: Int?
        
        public var company: String?
        
        public var userId: String?
        
        public var layoutName: String?
        
        public var isDefault: Bool?
        
        public var layoutConfig: LayoutConfigDetails?
        
        public var config: LayoutDetailConfig?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case company = "company"
            
            case userId = "user_id"
            
            case layoutName = "layout_name"
            
            case isDefault = "is_default"
            
            case layoutConfig = "layout_config"
            
            case config = "config"
            
        }

        public init(company: String? = nil, config: LayoutDetailConfig? = nil, id: Int? = nil, isDefault: Bool? = nil, layoutConfig: LayoutConfigDetails? = nil, layoutName: String? = nil, userId: String? = nil) {
            
            self.id = id
            
            self.company = company
            
            self.userId = userId
            
            self.layoutName = layoutName
            
            self.isDefault = isDefault
            
            self.layoutConfig = layoutConfig
            
            self.config = config
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(Int.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    company = try container.decode(String.self, forKey: .company)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    userId = try container.decode(String.self, forKey: .userId)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    layoutName = try container.decode(String.self, forKey: .layoutName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isDefault = try container.decode(Bool.self, forKey: .isDefault)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    layoutConfig = try container.decode(LayoutConfigDetails.self, forKey: .layoutConfig)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    config = try container.decode(LayoutDetailConfig.self, forKey: .config)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(company, forKey: .company)
            
            
            
            
            try? container.encodeIfPresent(userId, forKey: .userId)
            
            
            
            
            try? container.encodeIfPresent(layoutName, forKey: .layoutName)
            
            
            
            
            try? container.encodeIfPresent(isDefault, forKey: .isDefault)
            
            
            
            
            try? container.encodeIfPresent(layoutConfig, forKey: .layoutConfig)
            
            
            
            
            try? container.encodeIfPresent(config, forKey: .config)
            
            
        }
        
    }
}




