

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ExportRequest
        Used By: Analytics
    */

    class ExportRequest: Codable {
        
        
        public var compareEndDate: String?
        
        public var compareStartDate: String?
        
        public var endDate: String?
        
        public var orderPlatform: String?
        
        public var orderStatus: String?
        
        public var screenSize: String?
        
        public var startDate: String?
        
        public var storeCode: String?
        
        public var valueType: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case compareEndDate = "compare_end_date"
            
            case compareStartDate = "compare_start_date"
            
            case endDate = "end_date"
            
            case orderPlatform = "order_platform"
            
            case orderStatus = "order_status"
            
            case screenSize = "screen_size"
            
            case startDate = "start_date"
            
            case storeCode = "store_code"
            
            case valueType = "value_type"
            
        }

        public init(compareEndDate: String? = nil, compareStartDate: String? = nil, endDate: String? = nil, orderPlatform: String? = nil, orderStatus: String? = nil, screenSize: String? = nil, startDate: String? = nil, storeCode: String? = nil, valueType: String? = nil) {
            
            self.compareEndDate = compareEndDate
            
            self.compareStartDate = compareStartDate
            
            self.endDate = endDate
            
            self.orderPlatform = orderPlatform
            
            self.orderStatus = orderStatus
            
            self.screenSize = screenSize
            
            self.startDate = startDate
            
            self.storeCode = storeCode
            
            self.valueType = valueType
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    compareEndDate = try container.decode(String.self, forKey: .compareEndDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    compareStartDate = try container.decode(String.self, forKey: .compareStartDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    endDate = try container.decode(String.self, forKey: .endDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    orderPlatform = try container.decode(String.self, forKey: .orderPlatform)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    orderStatus = try container.decode(String.self, forKey: .orderStatus)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    screenSize = try container.decode(String.self, forKey: .screenSize)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    startDate = try container.decode(String.self, forKey: .startDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    storeCode = try container.decode(String.self, forKey: .storeCode)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    valueType = try container.decode(String.self, forKey: .valueType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(compareEndDate, forKey: .compareEndDate)
            
            
            
            
            try? container.encodeIfPresent(compareStartDate, forKey: .compareStartDate)
            
            
            
            
            try? container.encodeIfPresent(endDate, forKey: .endDate)
            
            
            
            
            try? container.encodeIfPresent(orderPlatform, forKey: .orderPlatform)
            
            
            
            
            try? container.encodeIfPresent(orderStatus, forKey: .orderStatus)
            
            
            
            
            try? container.encodeIfPresent(screenSize, forKey: .screenSize)
            
            
            
            
            try? container.encodeIfPresent(startDate, forKey: .startDate)
            
            
            
            
            try? container.encodeIfPresent(storeCode, forKey: .storeCode)
            
            
            
            
            try? container.encodeIfPresent(valueType, forKey: .valueType)
            
            
        }
        
    }
}




