

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: StatFiltersRequest
        Used By: Analytics
    */

    class StatFiltersRequest: Codable {
        
        
        public var filters: Filters?
        

        public enum CodingKeys: String, CodingKey {
            
            case filters = "filters"
            
        }

        public init(filters: Filters? = nil) {
            
            self.filters = filters
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    filters = try container.decode(Filters.self, forKey: .filters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(filters, forKey: .filters)
            
            
        }
        
    }
}




