

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ConditionalFilterData
        Used By: Analytics
    */

    class ConditionalFilterData: Codable {
        
        
        public var rows: [Row]?
        
        public var limit: Int?
        

        public enum CodingKeys: String, CodingKey {
            
            case rows = "rows"
            
            case limit = "limit"
            
        }

        public init(limit: Int? = nil, rows: [Row]? = nil) {
            
            self.rows = rows
            
            self.limit = limit
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    rows = try container.decode([Row].self, forKey: .rows)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    limit = try container.decode(Int.self, forKey: .limit)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(rows, forKey: .rows)
            
            
            
            
            try? container.encodeIfPresent(limit, forKey: .limit)
            
            
        }
        
    }
}




