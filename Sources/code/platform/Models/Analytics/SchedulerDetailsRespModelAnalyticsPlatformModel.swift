

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: SchedulerDetailsRespModel
        Used By: Analytics
    */

    class SchedulerDetailsRespModel: Codable {
        
        
        public var headers: [Headers]?
        
        public var records: [ReportItem]?
        

        public enum CodingKeys: String, CodingKey {
            
            case headers = "headers"
            
            case records = "records"
            
        }

        public init(headers: [Headers]? = nil, records: [ReportItem]? = nil) {
            
            self.headers = headers
            
            self.records = records
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    headers = try container.decode([Headers].self, forKey: .headers)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    records = try container.decode([ReportItem].self, forKey: .records)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(headers, forKey: .headers)
            
            
            
            
            try? container.encodeIfPresent(records, forKey: .records)
            
            
        }
        
    }
}




