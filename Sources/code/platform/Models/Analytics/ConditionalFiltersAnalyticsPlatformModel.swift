

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ConditionalFilters
        Used By: Analytics
    */

    class ConditionalFilters: Codable {
        
        
        public var blocks: [Block]?
        
        public var logicalOperator: String?
        
        public var valid: Bool?
        

        public enum CodingKeys: String, CodingKey {
            
            case blocks = "blocks"
            
            case logicalOperator = "logical_operator"
            
            case valid = "valid"
            
        }

        public init(blocks: [Block]? = nil, logicalOperator: String? = nil, valid: Bool? = nil) {
            
            self.blocks = blocks
            
            self.logicalOperator = logicalOperator
            
            self.valid = valid
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    blocks = try container.decode([Block].self, forKey: .blocks)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    logicalOperator = try container.decode(String.self, forKey: .logicalOperator)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    valid = try container.decode(Bool.self, forKey: .valid)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(blocks, forKey: .blocks)
            
            
            
            
            try? container.encodeIfPresent(logicalOperator, forKey: .logicalOperator)
            
            
            
            
            try? container.encodeIfPresent(valid, forKey: .valid)
            
            
        }
        
    }
}




