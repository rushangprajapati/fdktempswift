

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: TemplateDetailsRequestBody
        Used By: Analytics
    */

    class TemplateDetailsRequestBody: Codable {
        
        
        public var filters: [GraphFilters]?
        
        public var sortBy: [Sort]?
        
        public var page: Page?
        

        public enum CodingKeys: String, CodingKey {
            
            case filters = "filters"
            
            case sortBy = "sort_by"
            
            case page = "page"
            
        }

        public init(filters: [GraphFilters]? = nil, page: Page? = nil, sortBy: [Sort]? = nil) {
            
            self.filters = filters
            
            self.sortBy = sortBy
            
            self.page = page
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    filters = try container.decode([GraphFilters].self, forKey: .filters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    sortBy = try container.decode([Sort].self, forKey: .sortBy)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    page = try container.decode(Page.self, forKey: .page)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(filters, forKey: .filters)
            
            
            
            
            try? container.encodeIfPresent(sortBy, forKey: .sortBy)
            
            
            
            
            try? container.encodeIfPresent(page, forKey: .page)
            
            
        }
        
    }
}




