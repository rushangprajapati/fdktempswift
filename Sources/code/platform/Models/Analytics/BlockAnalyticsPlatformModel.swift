

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Block
        Used By: Analytics
    */

    class Block: Codable {
        
        
        public var columnName: String?
        
        public var conditionalOperator: String?
        
        public var value: String?
        
        public var type: String?
        
        public var logicalOperator: String?
        
        public var valid: Bool?
        

        public enum CodingKeys: String, CodingKey {
            
            case columnName = "column_name"
            
            case conditionalOperator = "conditional_operator"
            
            case value = "value"
            
            case type = "type"
            
            case logicalOperator = "logical_operator"
            
            case valid = "valid"
            
        }

        public init(columnName: String? = nil, conditionalOperator: String? = nil, logicalOperator: String? = nil, type: String? = nil, valid: Bool? = nil, value: String? = nil) {
            
            self.columnName = columnName
            
            self.conditionalOperator = conditionalOperator
            
            self.value = value
            
            self.type = type
            
            self.logicalOperator = logicalOperator
            
            self.valid = valid
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    columnName = try container.decode(String.self, forKey: .columnName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    conditionalOperator = try container.decode(String.self, forKey: .conditionalOperator)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    value = try container.decode(String.self, forKey: .value)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    type = try container.decode(String.self, forKey: .type)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    logicalOperator = try container.decode(String.self, forKey: .logicalOperator)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    valid = try container.decode(Bool.self, forKey: .valid)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(columnName, forKey: .columnName)
            
            
            
            
            try? container.encodeIfPresent(conditionalOperator, forKey: .conditionalOperator)
            
            
            
            
            try? container.encodeIfPresent(value, forKey: .value)
            
            
            
            
            try? container.encodeIfPresent(type, forKey: .type)
            
            
            
            
            try? container.encodeIfPresent(logicalOperator, forKey: .logicalOperator)
            
            
            
            
            try? container.encodeIfPresent(valid, forKey: .valid)
            
            
        }
        
    }
}




