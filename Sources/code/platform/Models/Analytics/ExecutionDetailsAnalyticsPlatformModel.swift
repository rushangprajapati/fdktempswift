

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ExecutionDetails
        Used By: Analytics
    */

    class ExecutionDetails: Codable {
        
        
        public var lastRunAt: Int?
        
        public var nextRunAt: Int?
        

        public enum CodingKeys: String, CodingKey {
            
            case lastRunAt = "last_run_at"
            
            case nextRunAt = "next_run_at"
            
        }

        public init(lastRunAt: Int? = nil, nextRunAt: Int? = nil) {
            
            self.lastRunAt = lastRunAt
            
            self.nextRunAt = nextRunAt
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    lastRunAt = try container.decode(Int.self, forKey: .lastRunAt)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    nextRunAt = try container.decode(Int.self, forKey: .nextRunAt)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(lastRunAt, forKey: .lastRunAt)
            
            
            
            
            try? container.encodeIfPresent(nextRunAt, forKey: .nextRunAt)
            
            
        }
        
    }
}




