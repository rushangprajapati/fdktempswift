

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ReportItemValue
        Used By: Analytics
    */

    class ReportItemValue: Codable {
        
        
        public var id: ValueNumber?
        
        public var name: ValueString?
        
        public var subject: ValueString?
        
        public var category: ValueCategory?
        
        public var createdAt: ValueString?
        
        public var status: ValueStatus?
        
        public var to: ValueTo?
        
        public var executionDetails: ExecutionDetails?
        
        public var frequency: Frequency?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case name = "name"
            
            case subject = "subject"
            
            case category = "category"
            
            case createdAt = "created_at"
            
            case status = "status"
            
            case to = "to"
            
            case executionDetails = "execution_details"
            
            case frequency = "frequency"
            
        }

        public init(category: ValueCategory? = nil, createdAt: ValueString? = nil, executionDetails: ExecutionDetails? = nil, frequency: Frequency? = nil, id: ValueNumber? = nil, name: ValueString? = nil, status: ValueStatus? = nil, subject: ValueString? = nil, to: ValueTo? = nil) {
            
            self.id = id
            
            self.name = name
            
            self.subject = subject
            
            self.category = category
            
            self.createdAt = createdAt
            
            self.status = status
            
            self.to = to
            
            self.executionDetails = executionDetails
            
            self.frequency = frequency
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(ValueNumber.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(ValueString.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    subject = try container.decode(ValueString.self, forKey: .subject)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    category = try container.decode(ValueCategory.self, forKey: .category)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    createdAt = try container.decode(ValueString.self, forKey: .createdAt)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    status = try container.decode(ValueStatus.self, forKey: .status)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    to = try container.decode(ValueTo.self, forKey: .to)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    executionDetails = try container.decode(ExecutionDetails.self, forKey: .executionDetails)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    frequency = try container.decode(Frequency.self, forKey: .frequency)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(subject, forKey: .subject)
            
            
            
            
            try? container.encodeIfPresent(category, forKey: .category)
            
            
            
            
            try? container.encodeIfPresent(createdAt, forKey: .createdAt)
            
            
            
            
            try? container.encodeIfPresent(status, forKey: .status)
            
            
            
            
            try? container.encodeIfPresent(to, forKey: .to)
            
            
            
            
            try? container.encodeIfPresent(executionDetails, forKey: .executionDetails)
            
            
            
            
            try? container.encodeIfPresent(frequency, forKey: .frequency)
            
            
        }
        
    }
}




