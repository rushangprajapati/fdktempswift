

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: StatFiltersItems
        Used By: Analytics
    */

    class StatFiltersItems: Codable {
        
        
        public var id: String?
        
        public var actualValue: Double?
        
        public var changeInValue: Double?
        
        public var trendMapping: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case actualValue = "actual_value"
            
            case changeInValue = "change_in_value"
            
            case trendMapping = "trend_mapping"
            
        }

        public init(actualValue: Double? = nil, changeInValue: Double? = nil, id: String? = nil, trendMapping: String? = nil) {
            
            self.id = id
            
            self.actualValue = actualValue
            
            self.changeInValue = changeInValue
            
            self.trendMapping = trendMapping
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(String.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    actualValue = try container.decode(Double.self, forKey: .actualValue)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    changeInValue = try container.decode(Double.self, forKey: .changeInValue)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    trendMapping = try container.decode(String.self, forKey: .trendMapping)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(actualValue, forKey: .actualValue)
            
            
            
            
            try? container.encodeIfPresent(changeInValue, forKey: .changeInValue)
            
            
            
            
            try? container.encodeIfPresent(trendMapping, forKey: .trendMapping)
            
            
        }
        
    }
}




