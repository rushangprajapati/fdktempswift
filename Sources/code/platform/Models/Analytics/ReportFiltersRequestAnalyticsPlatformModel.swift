

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ReportFiltersRequest
        Used By: Analytics
    */

    class ReportFiltersRequest: Codable {
        
        
        public var screenSize: String?
        
        public var page: Page?
        
        public var sort: [Sort]?
        
        public var columns: [Columns]?
        
        public var filters: Filters?
        

        public enum CodingKeys: String, CodingKey {
            
            case screenSize = "screen_size"
            
            case page = "page"
            
            case sort = "sort"
            
            case columns = "columns"
            
            case filters = "filters"
            
        }

        public init(columns: [Columns]? = nil, filters: Filters? = nil, page: Page? = nil, screenSize: String? = nil, sort: [Sort]? = nil) {
            
            self.screenSize = screenSize
            
            self.page = page
            
            self.sort = sort
            
            self.columns = columns
            
            self.filters = filters
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    screenSize = try container.decode(String.self, forKey: .screenSize)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    page = try container.decode(Page.self, forKey: .page)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    sort = try container.decode([Sort].self, forKey: .sort)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    columns = try container.decode([Columns].self, forKey: .columns)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    filters = try container.decode(Filters.self, forKey: .filters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(screenSize, forKey: .screenSize)
            
            
            
            
            try? container.encodeIfPresent(page, forKey: .page)
            
            
            
            
            try? container.encodeIfPresent(sort, forKey: .sort)
            
            
            
            
            try? container.encodeIfPresent(columns, forKey: .columns)
            
            
            
            
            try? container.encodeIfPresent(filters, forKey: .filters)
            
            
        }
        
    }
}




