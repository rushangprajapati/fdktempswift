

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Row
        Used By: Analytics
    */

    class Row: Codable {
        
        
        public var columnName: String?
        
        public var type: String?
        
        public var datasource: String?
        
        public var name: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case columnName = "column_name"
            
            case type = "type"
            
            case datasource = "datasource"
            
            case name = "name"
            
        }

        public init(columnName: String? = nil, datasource: String? = nil, name: String? = nil, type: String? = nil) {
            
            self.columnName = columnName
            
            self.type = type
            
            self.datasource = datasource
            
            self.name = name
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    columnName = try container.decode(String.self, forKey: .columnName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    type = try container.decode(String.self, forKey: .type)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    datasource = try container.decode(String.self, forKey: .datasource)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(String.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(columnName, forKey: .columnName)
            
            
            
            
            try? container.encodeIfPresent(type, forKey: .type)
            
            
            
            
            try? container.encodeIfPresent(datasource, forKey: .datasource)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
        }
        
    }
}




