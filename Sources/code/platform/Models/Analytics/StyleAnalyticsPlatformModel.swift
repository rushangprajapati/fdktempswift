

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Style
        Used By: Analytics
    */

    class Style: Codable {
        
        
        public var maxLength: Int?
        

        public enum CodingKeys: String, CodingKey {
            
            case maxLength = "max_length"
            
        }

        public init(maxLength: Int? = nil) {
            
            self.maxLength = maxLength
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    maxLength = try container.decode(Int.self, forKey: .maxLength)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(maxLength, forKey: .maxLength)
            
            
        }
        
    }
}




