

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ReportItem
        Used By: Analytics
    */

    class ReportItem: Codable {
        
        
        public var id: IntegerItem?
        
        public var name: StringItem?
        
        public var seqNum: IntegerItem?
        
        public var category: Category?
        
        public var lastViewed: StringItem?
        
        public var userName: StringItem?
        
        public var config: ReportItemConfig?
        
        public var type: StringItem?
        
        public var isDefault: BooleanItem?
        
        public var action: Action?
        
        public var values: ReportItemValue?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case name = "name"
            
            case seqNum = "seq_num"
            
            case category = "category"
            
            case lastViewed = "last_viewed"
            
            case userName = "user_name"
            
            case config = "config"
            
            case type = "type"
            
            case isDefault = "is_default"
            
            case action = "action"
            
            case values = "values"
            
        }

        public init(action: Action? = nil, category: Category? = nil, config: ReportItemConfig? = nil, id: IntegerItem? = nil, isDefault: BooleanItem? = nil, lastViewed: StringItem? = nil, name: StringItem? = nil, seqNum: IntegerItem? = nil, type: StringItem? = nil, userName: StringItem? = nil, values: ReportItemValue? = nil) {
            
            self.id = id
            
            self.name = name
            
            self.seqNum = seqNum
            
            self.category = category
            
            self.lastViewed = lastViewed
            
            self.userName = userName
            
            self.config = config
            
            self.type = type
            
            self.isDefault = isDefault
            
            self.action = action
            
            self.values = values
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(IntegerItem.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(StringItem.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    seqNum = try container.decode(IntegerItem.self, forKey: .seqNum)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    category = try container.decode(Category.self, forKey: .category)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    lastViewed = try container.decode(StringItem.self, forKey: .lastViewed)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    userName = try container.decode(StringItem.self, forKey: .userName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    config = try container.decode(ReportItemConfig.self, forKey: .config)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    type = try container.decode(StringItem.self, forKey: .type)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isDefault = try container.decode(BooleanItem.self, forKey: .isDefault)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    action = try container.decode(Action.self, forKey: .action)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    values = try container.decode(ReportItemValue.self, forKey: .values)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(seqNum, forKey: .seqNum)
            
            
            
            
            try? container.encodeIfPresent(category, forKey: .category)
            
            
            
            
            try? container.encodeIfPresent(lastViewed, forKey: .lastViewed)
            
            
            
            
            try? container.encodeIfPresent(userName, forKey: .userName)
            
            
            
            
            try? container.encodeIfPresent(config, forKey: .config)
            
            
            
            
            try? container.encodeIfPresent(type, forKey: .type)
            
            
            
            
            try? container.encodeIfPresent(isDefault, forKey: .isDefault)
            
            
            
            
            try? container.encodeIfPresent(action, forKey: .action)
            
            
            
            
            try? container.encodeIfPresent(values, forKey: .values)
            
            
        }
        
    }
}




