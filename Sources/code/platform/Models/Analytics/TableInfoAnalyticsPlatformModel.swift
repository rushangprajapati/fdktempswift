

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: TableInfo
        Used By: Analytics
    */

    class TableInfo: Codable {
        
        
        public var datasource: String?
        
        public var columns: [Columns]?
        
        public var selectedColumns: [SelectedColumns]?
        

        public enum CodingKeys: String, CodingKey {
            
            case datasource = "datasource"
            
            case columns = "columns"
            
            case selectedColumns = "selected_columns"
            
        }

        public init(columns: [Columns]? = nil, datasource: String? = nil, selectedColumns: [SelectedColumns]? = nil) {
            
            self.datasource = datasource
            
            self.columns = columns
            
            self.selectedColumns = selectedColumns
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    datasource = try container.decode(String.self, forKey: .datasource)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    columns = try container.decode([Columns].self, forKey: .columns)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    selectedColumns = try container.decode([SelectedColumns].self, forKey: .selectedColumns)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(datasource, forKey: .datasource)
            
            
            
            
            try? container.encodeIfPresent(columns, forKey: .columns)
            
            
            
            
            try? container.encodeIfPresent(selectedColumns, forKey: .selectedColumns)
            
            
        }
        
    }
}




