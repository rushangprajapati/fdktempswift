

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: EmailDetails
        Used By: Analytics
    */

    class EmailDetails: Codable {
        
        
        public var id: Int?
        
        public var to: [String]?
        
        public var cc: [String]?
        
        public var bcc: [String]?
        
        public var subject: String?
        
        public var body: String?
        
        public var attachmentType: String?
        
        public var reportTemplatesIds: [Int]?
        
        public var reportTemplates: [ReportTemplate]?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case to = "to"
            
            case cc = "cc"
            
            case bcc = "bcc"
            
            case subject = "subject"
            
            case body = "body"
            
            case attachmentType = "attachment_type"
            
            case reportTemplatesIds = "report_templates_ids"
            
            case reportTemplates = "report_templates"
            
        }

        public init(attachmentType: String? = nil, bcc: [String]? = nil, body: String? = nil, cc: [String]? = nil, id: Int? = nil, reportTemplates: [ReportTemplate]? = nil, reportTemplatesIds: [Int]? = nil, subject: String? = nil, to: [String]? = nil) {
            
            self.id = id
            
            self.to = to
            
            self.cc = cc
            
            self.bcc = bcc
            
            self.subject = subject
            
            self.body = body
            
            self.attachmentType = attachmentType
            
            self.reportTemplatesIds = reportTemplatesIds
            
            self.reportTemplates = reportTemplates
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(Int.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    to = try container.decode([String].self, forKey: .to)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    cc = try container.decode([String].self, forKey: .cc)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    bcc = try container.decode([String].self, forKey: .bcc)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    subject = try container.decode(String.self, forKey: .subject)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    body = try container.decode(String.self, forKey: .body)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    attachmentType = try container.decode(String.self, forKey: .attachmentType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportTemplatesIds = try container.decode([Int].self, forKey: .reportTemplatesIds)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportTemplates = try container.decode([ReportTemplate].self, forKey: .reportTemplates)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(to, forKey: .to)
            
            
            
            
            try? container.encodeIfPresent(cc, forKey: .cc)
            
            
            
            
            try? container.encodeIfPresent(bcc, forKey: .bcc)
            
            
            
            
            try? container.encodeIfPresent(subject, forKey: .subject)
            
            
            
            
            try? container.encodeIfPresent(body, forKey: .body)
            
            
            
            
            try? container.encodeIfPresent(attachmentType, forKey: .attachmentType)
            
            
            
            
            try? container.encodeIfPresent(reportTemplatesIds, forKey: .reportTemplatesIds)
            
            
            
            
            try? container.encodeIfPresent(reportTemplates, forKey: .reportTemplates)
            
            
        }
        
    }
}




