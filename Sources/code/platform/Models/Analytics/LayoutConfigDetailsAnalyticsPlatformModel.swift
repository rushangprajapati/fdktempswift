

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: LayoutConfigDetails
        Used By: Analytics
    */

    class LayoutConfigDetails: Codable {
        
        
        public var category: String?
        
        public var displayName: String?
        
        public var reportName: String?
        
        public var id: String?
        
        public var tableInfo: TableInfo?
        
        public var sidebarFilters: [[String: Any]]?
        
        public var conditionalFiltersData: ConditionalFilterData?
        
        public var reportFilters: [ReportFilters]?
        
        public var globalFilters: [GlobalFilters]?
        
        public var layoutName: String?
        
        public var isDefault: Bool?
        
        public var showTooltip: Bool?
        
        public var tooltipText: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case category = "category"
            
            case displayName = "display_name"
            
            case reportName = "report_name"
            
            case id = "id"
            
            case tableInfo = "table_info"
            
            case sidebarFilters = "sidebar_filters"
            
            case conditionalFiltersData = "conditional_filters_data"
            
            case reportFilters = "report_filters"
            
            case globalFilters = "global_filters"
            
            case layoutName = "layout_name"
            
            case isDefault = "is_default"
            
            case showTooltip = "show_tooltip"
            
            case tooltipText = "tooltip_text"
            
        }

        public init(category: String? = nil, conditionalFiltersData: ConditionalFilterData? = nil, displayName: String? = nil, globalFilters: [GlobalFilters]? = nil, id: String? = nil, isDefault: Bool? = nil, layoutName: String? = nil, reportFilters: [ReportFilters]? = nil, reportName: String? = nil, showTooltip: Bool? = nil, sidebarFilters: [[String: Any]]? = nil, tableInfo: TableInfo? = nil, tooltipText: String? = nil) {
            
            self.category = category
            
            self.displayName = displayName
            
            self.reportName = reportName
            
            self.id = id
            
            self.tableInfo = tableInfo
            
            self.sidebarFilters = sidebarFilters
            
            self.conditionalFiltersData = conditionalFiltersData
            
            self.reportFilters = reportFilters
            
            self.globalFilters = globalFilters
            
            self.layoutName = layoutName
            
            self.isDefault = isDefault
            
            self.showTooltip = showTooltip
            
            self.tooltipText = tooltipText
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    category = try container.decode(String.self, forKey: .category)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    displayName = try container.decode(String.self, forKey: .displayName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportName = try container.decode(String.self, forKey: .reportName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    id = try container.decode(String.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    tableInfo = try container.decode(TableInfo.self, forKey: .tableInfo)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    sidebarFilters = try container.decode([[String: Any]].self, forKey: .sidebarFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    conditionalFiltersData = try container.decode(ConditionalFilterData.self, forKey: .conditionalFiltersData)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportFilters = try container.decode([ReportFilters].self, forKey: .reportFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    globalFilters = try container.decode([GlobalFilters].self, forKey: .globalFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    layoutName = try container.decode(String.self, forKey: .layoutName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isDefault = try container.decode(Bool.self, forKey: .isDefault)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showTooltip = try container.decode(Bool.self, forKey: .showTooltip)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    tooltipText = try container.decode(String.self, forKey: .tooltipText)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(category, forKey: .category)
            
            
            
            
            try? container.encodeIfPresent(displayName, forKey: .displayName)
            
            
            
            
            try? container.encodeIfPresent(reportName, forKey: .reportName)
            
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(tableInfo, forKey: .tableInfo)
            
            
            
            
            try? container.encodeIfPresent(sidebarFilters, forKey: .sidebarFilters)
            
            
            
            
            try? container.encodeIfPresent(conditionalFiltersData, forKey: .conditionalFiltersData)
            
            
            
            
            try? container.encodeIfPresent(reportFilters, forKey: .reportFilters)
            
            
            
            
            try? container.encodeIfPresent(globalFilters, forKey: .globalFilters)
            
            
            
            
            try? container.encodeIfPresent(layoutName, forKey: .layoutName)
            
            
            
            
            try? container.encodeIfPresent(isDefault, forKey: .isDefault)
            
            
            
            
            try? container.encodeIfPresent(showTooltip, forKey: .showTooltip)
            
            
            
            
            try? container.encodeIfPresent(tooltipText, forKey: .tooltipText)
            
            
        }
        
    }
}




