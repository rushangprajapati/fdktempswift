

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: GlobalFilters
        Used By: Analytics
    */

    class GlobalFilters: Codable {
        
        
        public var id: String?
        
        public var name: String?
        
        public var placeholder: String?
        
        public var searchable: Bool?
        
        public var showCollapse: Bool?
        
        public var showTags: Bool?
        
        public var showClear: Bool?
        
        public var isDefaultRequired: Bool?
        
        public var dataSource: Bool?
        
        public var showCompare: Bool?
        
        public var allowedShortcuts: [String]?
        
        public var selectAllEnabled: Bool?
        
        public var defaultAllSelected: Bool?
        
        public var isMultiselect: Bool?
        
        public var reset: Bool?
        
        public var endDate: String?
        
        public var startDate: String?
        
        public var shortcut: String?
        
        public var salesChannel: [String]?
        
        public var timeFrameClause: String?
        
        public var compareStartDate: String?
        
        public var compareEndDate: String?
        
        public var isHidden: Bool?
        
        public var type: String?
        
        public var range: [Range]?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case name = "name"
            
            case placeholder = "placeholder"
            
            case searchable = "searchable"
            
            case showCollapse = "show_collapse"
            
            case showTags = "show_tags"
            
            case showClear = "show_clear"
            
            case isDefaultRequired = "is_default_required"
            
            case dataSource = "data_source"
            
            case showCompare = "show_compare"
            
            case allowedShortcuts = "allowed_shortcuts"
            
            case selectAllEnabled = "select_all_enabled"
            
            case defaultAllSelected = "default_all_selected"
            
            case isMultiselect = "is_multiselect"
            
            case reset = "reset"
            
            case endDate = "end_date"
            
            case startDate = "start_date"
            
            case shortcut = "shortcut"
            
            case salesChannel = "sales_channel"
            
            case timeFrameClause = "time_frame_clause"
            
            case compareStartDate = "compare_start_date"
            
            case compareEndDate = "compare_end_date"
            
            case isHidden = "is_hidden"
            
            case type = "type"
            
            case range = "range"
            
        }

        public init(allowedShortcuts: [String]? = nil, compareEndDate: String? = nil, compareStartDate: String? = nil, dataSource: Bool? = nil, defaultAllSelected: Bool? = nil, endDate: String? = nil, id: String? = nil, isDefaultRequired: Bool? = nil, isHidden: Bool? = nil, isMultiselect: Bool? = nil, name: String? = nil, placeholder: String? = nil, range: [Range]? = nil, reset: Bool? = nil, salesChannel: [String]? = nil, searchable: Bool? = nil, selectAllEnabled: Bool? = nil, shortcut: String? = nil, showClear: Bool? = nil, showCollapse: Bool? = nil, showCompare: Bool? = nil, showTags: Bool? = nil, startDate: String? = nil, timeFrameClause: String? = nil, type: String? = nil) {
            
            self.id = id
            
            self.name = name
            
            self.placeholder = placeholder
            
            self.searchable = searchable
            
            self.showCollapse = showCollapse
            
            self.showTags = showTags
            
            self.showClear = showClear
            
            self.isDefaultRequired = isDefaultRequired
            
            self.dataSource = dataSource
            
            self.showCompare = showCompare
            
            self.allowedShortcuts = allowedShortcuts
            
            self.selectAllEnabled = selectAllEnabled
            
            self.defaultAllSelected = defaultAllSelected
            
            self.isMultiselect = isMultiselect
            
            self.reset = reset
            
            self.endDate = endDate
            
            self.startDate = startDate
            
            self.shortcut = shortcut
            
            self.salesChannel = salesChannel
            
            self.timeFrameClause = timeFrameClause
            
            self.compareStartDate = compareStartDate
            
            self.compareEndDate = compareEndDate
            
            self.isHidden = isHidden
            
            self.type = type
            
            self.range = range
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(String.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(String.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    placeholder = try container.decode(String.self, forKey: .placeholder)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    searchable = try container.decode(Bool.self, forKey: .searchable)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showCollapse = try container.decode(Bool.self, forKey: .showCollapse)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showTags = try container.decode(Bool.self, forKey: .showTags)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showClear = try container.decode(Bool.self, forKey: .showClear)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isDefaultRequired = try container.decode(Bool.self, forKey: .isDefaultRequired)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    dataSource = try container.decode(Bool.self, forKey: .dataSource)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showCompare = try container.decode(Bool.self, forKey: .showCompare)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    allowedShortcuts = try container.decode([String].self, forKey: .allowedShortcuts)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    selectAllEnabled = try container.decode(Bool.self, forKey: .selectAllEnabled)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    defaultAllSelected = try container.decode(Bool.self, forKey: .defaultAllSelected)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isMultiselect = try container.decode(Bool.self, forKey: .isMultiselect)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reset = try container.decode(Bool.self, forKey: .reset)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    endDate = try container.decode(String.self, forKey: .endDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    startDate = try container.decode(String.self, forKey: .startDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    shortcut = try container.decode(String.self, forKey: .shortcut)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    salesChannel = try container.decode([String].self, forKey: .salesChannel)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    timeFrameClause = try container.decode(String.self, forKey: .timeFrameClause)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    compareStartDate = try container.decode(String.self, forKey: .compareStartDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    compareEndDate = try container.decode(String.self, forKey: .compareEndDate)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isHidden = try container.decode(Bool.self, forKey: .isHidden)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    type = try container.decode(String.self, forKey: .type)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    range = try container.decode([Range].self, forKey: .range)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(placeholder, forKey: .placeholder)
            
            
            
            
            try? container.encodeIfPresent(searchable, forKey: .searchable)
            
            
            
            
            try? container.encodeIfPresent(showCollapse, forKey: .showCollapse)
            
            
            
            
            try? container.encodeIfPresent(showTags, forKey: .showTags)
            
            
            
            
            try? container.encodeIfPresent(showClear, forKey: .showClear)
            
            
            
            
            try? container.encodeIfPresent(isDefaultRequired, forKey: .isDefaultRequired)
            
            
            
            
            try? container.encodeIfPresent(dataSource, forKey: .dataSource)
            
            
            
            
            try? container.encodeIfPresent(showCompare, forKey: .showCompare)
            
            
            
            
            try? container.encodeIfPresent(allowedShortcuts, forKey: .allowedShortcuts)
            
            
            
            
            try? container.encodeIfPresent(selectAllEnabled, forKey: .selectAllEnabled)
            
            
            
            
            try? container.encodeIfPresent(defaultAllSelected, forKey: .defaultAllSelected)
            
            
            
            
            try? container.encodeIfPresent(isMultiselect, forKey: .isMultiselect)
            
            
            
            
            try? container.encodeIfPresent(reset, forKey: .reset)
            
            
            
            
            try? container.encodeIfPresent(endDate, forKey: .endDate)
            
            
            
            
            try? container.encodeIfPresent(startDate, forKey: .startDate)
            
            
            
            
            try? container.encodeIfPresent(shortcut, forKey: .shortcut)
            
            
            
            
            try? container.encodeIfPresent(salesChannel, forKey: .salesChannel)
            
            
            
            
            try? container.encodeIfPresent(timeFrameClause, forKey: .timeFrameClause)
            
            
            
            
            try? container.encodeIfPresent(compareStartDate, forKey: .compareStartDate)
            
            
            
            
            try? container.encodeIfPresent(compareEndDate, forKey: .compareEndDate)
            
            
            
            
            try? container.encodeIfPresent(isHidden, forKey: .isHidden)
            
            
            
            
            try? container.encodeIfPresent(type, forKey: .type)
            
            
            
            
            try? container.encodeIfPresent(range, forKey: .range)
            
            
        }
        
    }
}




