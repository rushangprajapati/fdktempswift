

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: BooleanItem
        Used By: Analytics
    */

    class BooleanItem: Codable {
        
        
        public var value: Bool?
        

        public enum CodingKeys: String, CodingKey {
            
            case value = "value"
            
        }

        public init(value: Bool? = nil) {
            
            self.value = value
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    value = try container.decode(Bool.self, forKey: .value)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(value, forKey: .value)
            
            
        }
        
    }
}




