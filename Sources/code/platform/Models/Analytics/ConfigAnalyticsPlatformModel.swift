

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Config
        Used By: Analytics
    */

    class Config: Codable {
        
        
        public var filters: Filters?
        
        public var selectedColumns: [SelectedColumns]?
        

        public enum CodingKeys: String, CodingKey {
            
            case filters = "filters"
            
            case selectedColumns = "selected_columns"
            
        }

        public init(filters: Filters? = nil, selectedColumns: [SelectedColumns]? = nil) {
            
            self.filters = filters
            
            self.selectedColumns = selectedColumns
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    filters = try container.decode(Filters.self, forKey: .filters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    selectedColumns = try container.decode([SelectedColumns].self, forKey: .selectedColumns)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(filters, forKey: .filters)
            
            
            
            
            try? container.encodeIfPresent(selectedColumns, forKey: .selectedColumns)
            
            
        }
        
    }
}




