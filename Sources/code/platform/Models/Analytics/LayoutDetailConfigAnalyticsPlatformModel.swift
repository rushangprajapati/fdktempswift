

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: LayoutDetailConfig
        Used By: Analytics
    */

    class LayoutDetailConfig: Codable {
        
        
        public var reportFilters: [ReportFilters]?
        
        public var exportFilters: [String]?
        
        public var selectedColumns: [SelectedColumns]?
        

        public enum CodingKeys: String, CodingKey {
            
            case reportFilters = "report_filters"
            
            case exportFilters = "export_filters"
            
            case selectedColumns = "selected_columns"
            
        }

        public init(exportFilters: [String]? = nil, reportFilters: [ReportFilters]? = nil, selectedColumns: [SelectedColumns]? = nil) {
            
            self.reportFilters = reportFilters
            
            self.exportFilters = exportFilters
            
            self.selectedColumns = selectedColumns
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    reportFilters = try container.decode([ReportFilters].self, forKey: .reportFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    exportFilters = try container.decode([String].self, forKey: .exportFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    selectedColumns = try container.decode([SelectedColumns].self, forKey: .selectedColumns)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(reportFilters, forKey: .reportFilters)
            
            
            
            
            try? container.encodeIfPresent(exportFilters, forKey: .exportFilters)
            
            
            
            
            try? container.encodeIfPresent(selectedColumns, forKey: .selectedColumns)
            
            
        }
        
    }
}




