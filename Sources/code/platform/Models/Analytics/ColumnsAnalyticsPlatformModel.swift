

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Columns
        Used By: Analytics
    */

    class Columns: Codable {
        
        
        public var config: SelectedColumnsConfig?
        
        public var displayName: String?
        
        public var name: String?
        
        public var sortName: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case config = "config"
            
            case displayName = "display_name"
            
            case name = "name"
            
            case sortName = "sort_name"
            
        }

        public init(config: SelectedColumnsConfig? = nil, displayName: String? = nil, name: String? = nil, sortName: String? = nil) {
            
            self.config = config
            
            self.displayName = displayName
            
            self.name = name
            
            self.sortName = sortName
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    config = try container.decode(SelectedColumnsConfig.self, forKey: .config)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    displayName = try container.decode(String.self, forKey: .displayName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(String.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    sortName = try container.decode(String.self, forKey: .sortName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(config, forKey: .config)
            
            
            
            
            try? container.encodeIfPresent(displayName, forKey: .displayName)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(sortName, forKey: .sortName)
            
            
        }
        
    }
}




