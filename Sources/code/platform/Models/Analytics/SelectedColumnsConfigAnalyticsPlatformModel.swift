

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: SelectedColumnsConfig
        Used By: Analytics
    */

    class SelectedColumnsConfig: Codable {
        
        
        public var sort: Bool?
        
        public var style: Style?
        
        public var format: Bool?
        
        public var defaultColumn: Bool?
        
        public var prefix: String?
        
        public var suffix: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case sort = "sort"
            
            case style = "style"
            
            case format = "format"
            
            case defaultColumn = "default_column"
            
            case prefix = "prefix"
            
            case suffix = "suffix"
            
        }

        public init(defaultColumn: Bool? = nil, format: Bool? = nil, prefix: String? = nil, sort: Bool? = nil, style: Style? = nil, suffix: String? = nil) {
            
            self.sort = sort
            
            self.style = style
            
            self.format = format
            
            self.defaultColumn = defaultColumn
            
            self.prefix = prefix
            
            self.suffix = suffix
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    sort = try container.decode(Bool.self, forKey: .sort)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    style = try container.decode(Style.self, forKey: .style)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    format = try container.decode(Bool.self, forKey: .format)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    defaultColumn = try container.decode(Bool.self, forKey: .defaultColumn)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    prefix = try container.decode(String.self, forKey: .prefix)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    suffix = try container.decode(String.self, forKey: .suffix)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(sort, forKey: .sort)
            
            
            
            
            try? container.encodeIfPresent(style, forKey: .style)
            
            
            
            
            try? container.encodeIfPresent(format, forKey: .format)
            
            
            
            
            try? container.encodeIfPresent(defaultColumn, forKey: .defaultColumn)
            
            
            
            
            try? container.encodeIfPresent(prefix, forKey: .prefix)
            
            
            
            
            try? container.encodeIfPresent(suffix, forKey: .suffix)
            
            
        }
        
    }
}




