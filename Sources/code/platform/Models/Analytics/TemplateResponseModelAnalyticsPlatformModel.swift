

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: TemplateResponseModel
        Used By: Analytics
    */

    class TemplateResponseModel: Codable {
        
        
        public var executionDetails: ExecutionDetails?
        
        public var schedulerDetails: SchedulerDetails?
        

        public enum CodingKeys: String, CodingKey {
            
            case executionDetails = "execution_details"
            
            case schedulerDetails = "scheduler_details"
            
        }

        public init(executionDetails: ExecutionDetails? = nil, schedulerDetails: SchedulerDetails? = nil) {
            
            self.executionDetails = executionDetails
            
            self.schedulerDetails = schedulerDetails
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    executionDetails = try container.decode(ExecutionDetails.self, forKey: .executionDetails)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    schedulerDetails = try container.decode(SchedulerDetails.self, forKey: .schedulerDetails)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(executionDetails, forKey: .executionDetails)
            
            
            
            
            try? container.encodeIfPresent(schedulerDetails, forKey: .schedulerDetails)
            
            
        }
        
    }
}




