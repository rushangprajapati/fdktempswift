

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Sort
        Used By: Analytics
    */

    class Sort: Codable {
        
        
        public var columnName: String?
        
        public var order: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case columnName = "column_name"
            
            case order = "order"
            
        }

        public init(columnName: String? = nil, order: String? = nil) {
            
            self.columnName = columnName
            
            self.order = order
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    columnName = try container.decode(String.self, forKey: .columnName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    order = try container.decode(String.self, forKey: .order)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(columnName, forKey: .columnName)
            
            
            
            
            try? container.encodeIfPresent(order, forKey: .order)
            
            
        }
        
    }
}




