

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: DashboardFilterRequest
        Used By: Analytics
    */

    class DashboardFilterRequest: Codable {
        
        
        public var screenSize: String?
        
        public var filters: Filters?
        

        public enum CodingKeys: String, CodingKey {
            
            case screenSize = "screen_size"
            
            case filters = "filters"
            
        }

        public init(filters: Filters? = nil, screenSize: String? = nil) {
            
            self.screenSize = screenSize
            
            self.filters = filters
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    screenSize = try container.decode(String.self, forKey: .screenSize)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    filters = try container.decode(Filters.self, forKey: .filters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(screenSize, forKey: .screenSize)
            
            
            
            
            try? container.encodeIfPresent(filters, forKey: .filters)
            
            
        }
        
    }
}




