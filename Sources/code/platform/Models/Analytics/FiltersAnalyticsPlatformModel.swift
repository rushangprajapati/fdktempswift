

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Filters
        Used By: Analytics
    */

    class Filters: Codable {
        
        
        public var graphFilter: [String]?
        
        public var screenSize: Double?
        
        public var reportFilters: [ReportFilters]?
        
        public var exportFilters: [String]?
        
        public var globalFilters: GlobalFilters?
        
        public var sidebarFilters: [[String: Any]]?
        
        public var conditionalFilters: [ConditionalFilters]?
        
        public var selectedColumns: [SelectedColumns]?
        

        public enum CodingKeys: String, CodingKey {
            
            case graphFilter = "graph_filter"
            
            case screenSize = "screen_size"
            
            case reportFilters = "report_filters"
            
            case exportFilters = "export_filters"
            
            case globalFilters = "global_filters"
            
            case sidebarFilters = "sidebar_filters"
            
            case conditionalFilters = "conditional_filters"
            
            case selectedColumns = "selected_columns"
            
        }

        public init(conditionalFilters: [ConditionalFilters]? = nil, exportFilters: [String]? = nil, globalFilters: GlobalFilters? = nil, graphFilter: [String]? = nil, reportFilters: [ReportFilters]? = nil, screenSize: Double? = nil, selectedColumns: [SelectedColumns]? = nil, sidebarFilters: [[String: Any]]? = nil) {
            
            self.graphFilter = graphFilter
            
            self.screenSize = screenSize
            
            self.reportFilters = reportFilters
            
            self.exportFilters = exportFilters
            
            self.globalFilters = globalFilters
            
            self.sidebarFilters = sidebarFilters
            
            self.conditionalFilters = conditionalFilters
            
            self.selectedColumns = selectedColumns
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    graphFilter = try container.decode([String].self, forKey: .graphFilter)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    screenSize = try container.decode(Double.self, forKey: .screenSize)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportFilters = try container.decode([ReportFilters].self, forKey: .reportFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    exportFilters = try container.decode([String].self, forKey: .exportFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    globalFilters = try container.decode(GlobalFilters.self, forKey: .globalFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    sidebarFilters = try container.decode([[String: Any]].self, forKey: .sidebarFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    conditionalFilters = try container.decode([ConditionalFilters].self, forKey: .conditionalFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    selectedColumns = try container.decode([SelectedColumns].self, forKey: .selectedColumns)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(graphFilter, forKey: .graphFilter)
            
            
            
            
            try? container.encodeIfPresent(screenSize, forKey: .screenSize)
            
            
            
            
            try? container.encodeIfPresent(reportFilters, forKey: .reportFilters)
            
            
            
            
            try? container.encodeIfPresent(exportFilters, forKey: .exportFilters)
            
            
            
            
            try? container.encodeIfPresent(globalFilters, forKey: .globalFilters)
            
            
            
            
            try? container.encodeIfPresent(sidebarFilters, forKey: .sidebarFilters)
            
            
            
            
            try? container.encodeIfPresent(conditionalFilters, forKey: .conditionalFilters)
            
            
            
            
            try? container.encodeIfPresent(selectedColumns, forKey: .selectedColumns)
            
            
        }
        
    }
}




