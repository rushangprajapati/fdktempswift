

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: LayoutConfig
        Used By: Analytics
    */

    class LayoutConfig: Codable {
        
        
        public var layoutDetails: Dlayout?
        
        public var globalFilters: [LayoutGlobalFilters]?
        

        public enum CodingKeys: String, CodingKey {
            
            case layoutDetails = "layout_details"
            
            case globalFilters = "global_filters"
            
        }

        public init(globalFilters: [LayoutGlobalFilters]? = nil, layoutDetails: Dlayout? = nil) {
            
            self.layoutDetails = layoutDetails
            
            self.globalFilters = globalFilters
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    layoutDetails = try container.decode(Dlayout.self, forKey: .layoutDetails)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    globalFilters = try container.decode([LayoutGlobalFilters].self, forKey: .globalFilters)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(layoutDetails, forKey: .layoutDetails)
            
            
            
            
            try? container.encodeIfPresent(globalFilters, forKey: .globalFilters)
            
            
        }
        
    }
}




