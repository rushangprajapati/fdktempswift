

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ReportFilters
        Used By: Analytics
    */

    class ReportFilters: Codable {
        
        
        public var id: String?
        
        public var name: String?
        
        public var placeholder: String?
        
        public var searchable: Bool?
        
        public var showCollapse: Bool?
        
        public var showTags: Bool?
        
        public var showClear: Bool?
        
        public var isDefaultRequired: Bool?
        
        public var dataSource: Bool?
        
        public var selectAllEnabled: Bool?
        
        public var defaultAllSelected: Bool?
        
        public var isMultiselect: Bool?
        
        public var reset: Bool?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case name = "name"
            
            case placeholder = "placeholder"
            
            case searchable = "searchable"
            
            case showCollapse = "show_collapse"
            
            case showTags = "show_tags"
            
            case showClear = "show_clear"
            
            case isDefaultRequired = "is_default_required"
            
            case dataSource = "data_source"
            
            case selectAllEnabled = "select_all_enabled"
            
            case defaultAllSelected = "default_all_selected"
            
            case isMultiselect = "is_multiselect"
            
            case reset = "reset"
            
        }

        public init(dataSource: Bool? = nil, defaultAllSelected: Bool? = nil, id: String? = nil, isDefaultRequired: Bool? = nil, isMultiselect: Bool? = nil, name: String? = nil, placeholder: String? = nil, reset: Bool? = nil, searchable: Bool? = nil, selectAllEnabled: Bool? = nil, showClear: Bool? = nil, showCollapse: Bool? = nil, showTags: Bool? = nil) {
            
            self.id = id
            
            self.name = name
            
            self.placeholder = placeholder
            
            self.searchable = searchable
            
            self.showCollapse = showCollapse
            
            self.showTags = showTags
            
            self.showClear = showClear
            
            self.isDefaultRequired = isDefaultRequired
            
            self.dataSource = dataSource
            
            self.selectAllEnabled = selectAllEnabled
            
            self.defaultAllSelected = defaultAllSelected
            
            self.isMultiselect = isMultiselect
            
            self.reset = reset
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(String.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(String.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    placeholder = try container.decode(String.self, forKey: .placeholder)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    searchable = try container.decode(Bool.self, forKey: .searchable)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showCollapse = try container.decode(Bool.self, forKey: .showCollapse)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showTags = try container.decode(Bool.self, forKey: .showTags)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    showClear = try container.decode(Bool.self, forKey: .showClear)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isDefaultRequired = try container.decode(Bool.self, forKey: .isDefaultRequired)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    dataSource = try container.decode(Bool.self, forKey: .dataSource)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    selectAllEnabled = try container.decode(Bool.self, forKey: .selectAllEnabled)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    defaultAllSelected = try container.decode(Bool.self, forKey: .defaultAllSelected)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isMultiselect = try container.decode(Bool.self, forKey: .isMultiselect)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reset = try container.decode(Bool.self, forKey: .reset)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(placeholder, forKey: .placeholder)
            
            
            
            
            try? container.encodeIfPresent(searchable, forKey: .searchable)
            
            
            
            
            try? container.encodeIfPresent(showCollapse, forKey: .showCollapse)
            
            
            
            
            try? container.encodeIfPresent(showTags, forKey: .showTags)
            
            
            
            
            try? container.encodeIfPresent(showClear, forKey: .showClear)
            
            
            
            
            try? container.encodeIfPresent(isDefaultRequired, forKey: .isDefaultRequired)
            
            
            
            
            try? container.encodeIfPresent(dataSource, forKey: .dataSource)
            
            
            
            
            try? container.encodeIfPresent(selectAllEnabled, forKey: .selectAllEnabled)
            
            
            
            
            try? container.encodeIfPresent(defaultAllSelected, forKey: .defaultAllSelected)
            
            
            
            
            try? container.encodeIfPresent(isMultiselect, forKey: .isMultiselect)
            
            
            
            
            try? container.encodeIfPresent(reset, forKey: .reset)
            
            
        }
        
    }
}




