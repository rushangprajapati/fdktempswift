

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ReportsDetailsRespModel
        Used By: Analytics
    */

    class ReportsDetailsRespModel: Codable {
        
        
        public var headers: [Headers]?
        
        public var records: [ReportItem]?
        
        public var page: Page?
        

        public enum CodingKeys: String, CodingKey {
            
            case headers = "headers"
            
            case records = "records"
            
            case page = "page"
            
        }

        public init(headers: [Headers]? = nil, page: Page? = nil, records: [ReportItem]? = nil) {
            
            self.headers = headers
            
            self.records = records
            
            self.page = page
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    headers = try container.decode([Headers].self, forKey: .headers)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    records = try container.decode([ReportItem].self, forKey: .records)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    page = try container.decode(Page.self, forKey: .page)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(headers, forKey: .headers)
            
            
            
            
            try? container.encodeIfPresent(records, forKey: .records)
            
            
            
            
            try? container.encodeIfPresent(page, forKey: .page)
            
            
        }
        
    }
}




