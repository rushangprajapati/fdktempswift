

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Dlayout
        Used By: Analytics
    */

    class Dlayout: Codable {
        
        
        public var dataSource: String?
        
        public var orders: LayoutString?
        
        public var inventory: LayoutString?
        
        public var logistics: LayoutString?
        
        public var finance: LayoutString?
        
        public var users: LayoutString?
        

        public enum CodingKeys: String, CodingKey {
            
            case dataSource = "data_source"
            
            case orders = "orders"
            
            case inventory = "inventory"
            
            case logistics = "logistics"
            
            case finance = "finance"
            
            case users = "users"
            
        }

        public init(dataSource: String? = nil, finance: LayoutString? = nil, inventory: LayoutString? = nil, logistics: LayoutString? = nil, orders: LayoutString? = nil, users: LayoutString? = nil) {
            
            self.dataSource = dataSource
            
            self.orders = orders
            
            self.inventory = inventory
            
            self.logistics = logistics
            
            self.finance = finance
            
            self.users = users
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    dataSource = try container.decode(String.self, forKey: .dataSource)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    orders = try container.decode(LayoutString.self, forKey: .orders)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    inventory = try container.decode(LayoutString.self, forKey: .inventory)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    logistics = try container.decode(LayoutString.self, forKey: .logistics)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    finance = try container.decode(LayoutString.self, forKey: .finance)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    users = try container.decode(LayoutString.self, forKey: .users)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(dataSource, forKey: .dataSource)
            
            
            
            
            try? container.encodeIfPresent(orders, forKey: .orders)
            
            
            
            
            try? container.encodeIfPresent(inventory, forKey: .inventory)
            
            
            
            
            try? container.encodeIfPresent(logistics, forKey: .logistics)
            
            
            
            
            try? container.encodeIfPresent(finance, forKey: .finance)
            
            
            
            
            try? container.encodeIfPresent(users, forKey: .users)
            
            
        }
        
    }
}




