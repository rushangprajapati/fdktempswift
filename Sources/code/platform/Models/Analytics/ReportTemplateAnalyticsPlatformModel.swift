

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: ReportTemplate
        Used By: Analytics
    */

    class ReportTemplate: Codable {
        
        
        public var id: Int?
        
        public var seqNum: Int?
        
        public var reportName: String?
        
        public var config: Config?
        
        public var reportType: String?
        
        public var category: String?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case seqNum = "seq_num"
            
            case reportName = "report_name"
            
            case config = "config"
            
            case reportType = "report_type"
            
            case category = "category"
            
        }

        public init(category: String? = nil, config: Config? = nil, id: Int? = nil, reportName: String? = nil, reportType: String? = nil, seqNum: Int? = nil) {
            
            self.id = id
            
            self.seqNum = seqNum
            
            self.reportName = reportName
            
            self.config = config
            
            self.reportType = reportType
            
            self.category = category
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(Int.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    seqNum = try container.decode(Int.self, forKey: .seqNum)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportName = try container.decode(String.self, forKey: .reportName)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    config = try container.decode(Config.self, forKey: .config)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportType = try container.decode(String.self, forKey: .reportType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    category = try container.decode(String.self, forKey: .category)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(seqNum, forKey: .seqNum)
            
            
            
            
            try? container.encodeIfPresent(reportName, forKey: .reportName)
            
            
            
            
            try? container.encodeIfPresent(config, forKey: .config)
            
            
            
            
            try? container.encodeIfPresent(reportType, forKey: .reportType)
            
            
            
            
            try? container.encodeIfPresent(category, forKey: .category)
            
            
        }
        
    }
}




