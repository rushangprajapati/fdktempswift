

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: SideBarRequest
        Used By: Analytics
    */

    class SideBarRequest: Codable {
        
        
        public var salesChannel: [String]?
        

        public enum CodingKeys: String, CodingKey {
            
            case salesChannel = "sales_channel"
            
        }

        public init(salesChannel: [String]? = nil) {
            
            self.salesChannel = salesChannel
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    salesChannel = try container.decode([String].self, forKey: .salesChannel)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(salesChannel, forKey: .salesChannel)
            
            
        }
        
    }
}




