

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: TemplateLayoutRespModel
        Used By: Analytics
    */

    class TemplateLayoutRespModel: Codable {
        
        
        public var id: Int?
        
        public var name: String?
        
        public var reportType: String?
        
        public var category: String?
        
        public var isEditable: Bool?
        
        public var layout: Layout?
        
        public var config: Config?
        

        public enum CodingKeys: String, CodingKey {
            
            case id = "id"
            
            case name = "name"
            
            case reportType = "report_type"
            
            case category = "category"
            
            case isEditable = "is_editable"
            
            case layout = "layout"
            
            case config = "config"
            
        }

        public init(category: String? = nil, config: Config? = nil, id: Int? = nil, isEditable: Bool? = nil, layout: Layout? = nil, name: String? = nil, reportType: String? = nil) {
            
            self.id = id
            
            self.name = name
            
            self.reportType = reportType
            
            self.category = category
            
            self.isEditable = isEditable
            
            self.layout = layout
            
            self.config = config
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    id = try container.decode(Int.self, forKey: .id)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    name = try container.decode(String.self, forKey: .name)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    reportType = try container.decode(String.self, forKey: .reportType)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    category = try container.decode(String.self, forKey: .category)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    isEditable = try container.decode(Bool.self, forKey: .isEditable)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    layout = try container.decode(Layout.self, forKey: .layout)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    config = try container.decode(Config.self, forKey: .config)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(id, forKey: .id)
            
            
            
            
            try? container.encodeIfPresent(name, forKey: .name)
            
            
            
            
            try? container.encodeIfPresent(reportType, forKey: .reportType)
            
            
            
            
            try? container.encodeIfPresent(category, forKey: .category)
            
            
            
            
            try? container.encodeIfPresent(isEditable, forKey: .isEditable)
            
            
            
            
            try? container.encodeIfPresent(layout, forKey: .layout)
            
            
            
            
            try? container.encodeIfPresent(config, forKey: .config)
            
            
        }
        
    }
}




