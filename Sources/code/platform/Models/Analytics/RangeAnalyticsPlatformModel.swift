

import Foundation


public extension PlatformClient.Analytics {
    /*
        Model: Range
        Used By: Analytics
    */

    class Range: Codable {
        
        
        public var text: String?
        
        public var value: String?
        
        public var minDuration: Int?
        
        public var maxDuration: Int?
        

        public enum CodingKeys: String, CodingKey {
            
            case text = "text"
            
            case value = "value"
            
            case minDuration = "min_duration"
            
            case maxDuration = "max_duration"
            
        }

        public init(maxDuration: Int? = nil, minDuration: Int? = nil, text: String? = nil, value: String? = nil) {
            
            self.text = text
            
            self.value = value
            
            self.minDuration = minDuration
            
            self.maxDuration = maxDuration
            
        }

        required public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            
                do {
                    text = try container.decode(String.self, forKey: .text)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    value = try container.decode(String.self, forKey: .value)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    minDuration = try container.decode(Int.self, forKey: .minDuration)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
            
                do {
                    maxDuration = try container.decode(Int.self, forKey: .maxDuration)
                
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                } catch {
                    
                }
                
            
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            
            
            
            try? container.encodeIfPresent(text, forKey: .text)
            
            
            
            
            try? container.encodeIfPresent(value, forKey: .value)
            
            
            
            
            try? container.encodeIfPresent(minDuration, forKey: .minDuration)
            
            
            
            
            try? container.encodeIfPresent(maxDuration, forKey: .maxDuration)
            
            
        }
        
    }
}




