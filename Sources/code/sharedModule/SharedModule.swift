//
//  File.swift
//  
//
//  Created by Rushang Prajapati on 21/03/24.
//

//import Nitrozen_SwiftUI
import Foundation
import UIKit

//extension String: NitrozenElementStringSelectableStyle {
//    public var selectionTitle: String { return self  }
//}

extension FDKClient.ApplicationClient.Catalog.ProductFiltersKey: Hashable {
    public static func == (lhs: FDKClient.ApplicationClient.Catalog.ProductFiltersKey, rhs: FDKClient.ApplicationClient.Catalog.ProductFiltersKey) -> Bool {
        lhs.name == rhs.name
    }

    public final func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}


extension FDKClient.ApplicationClient.Catalog.ProductFilters: Hashable, Identifiable {
    public static func == (lhs: FDKClient.ApplicationClient.Catalog.ProductFilters, rhs: FDKClient.ApplicationClient.Catalog.ProductFilters) -> Bool {
        return lhs.key.name == rhs.key.name
    }
    public func hash(into hasher: inout Hasher) {
      hasher.combine(key.name)
    }
}


extension FDKClient.ApplicationClient.Catalog.ProductListingDetail: Hashable {
    public static func == (lhs: FDKClient.ApplicationClient.Catalog.ProductListingDetail, rhs: FDKClient.ApplicationClient.Catalog.ProductListingDetail) -> Bool {
        return lhs.uid == rhs.uid
    }
    
    public func hash(into hasher: inout Hasher) {
      hasher.combine(uid)
    }
}

extension FDKClient.ApplicationClient.Catalog.ProductSortOn: Hashable {
    public static func == (lhs: FDKClient.ApplicationClient.Catalog.ProductSortOn, rhs: FDKClient.ApplicationClient.Catalog.ProductSortOn) -> Bool {
        return lhs.name == rhs.name
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
    
}

extension FDKClient.ApplicationClient.Catalog.ProductFiltersValue: Hashable {
    public static func == (lhs: FDKClient.ApplicationClient.Catalog.ProductFiltersValue, rhs: FDKClient.ApplicationClient.Catalog.ProductFiltersValue) -> Bool {
        return lhs.value == rhs.value
    }
    
    public func hash(into hasher: inout Hasher) {
      hasher.combine(value)
    }
}

extension FDKClient.PlatformClient.ApplicationClient.Cart.Coupon: Hashable {
    public static func == (lhs: FDKClient.PlatformClient.ApplicationClient.Cart.Coupon, rhs: FDKClient.PlatformClient.ApplicationClient.Cart.Coupon) -> Bool {
        lhs.couponCode == rhs.couponCode
    }
    
    public func hash(into hasher: inout Hasher) {
       hasher.combine(couponCode)
    }
}

// MARK: Extra
extension FDKClient.PlatformClient.Order.SubLane: Equatable {
    public static func == (lhs: FDKClient.PlatformClient.Order.SubLane, rhs: FDKClient.PlatformClient.Order.SubLane) -> Bool {
        lhs.value == rhs.value
    }
}

extension FDKClient.PlatformClient.Order.SubLane: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(value)
    }
}

extension FDKClient.PlatformClient.ApplicationClient.Cart.DisplayBreakup: Hashable {
    public static func == (lhs: FDKClient.PlatformClient.ApplicationClient.Cart.DisplayBreakup, rhs: FDKClient.PlatformClient.ApplicationClient.Cart.DisplayBreakup) -> Bool {
        lhs.display == rhs.display
    }
        
    public func hash(into hasher: inout Hasher) {
       hasher.combine(display)
    }
}

extension FDKClient.PlatformClient.ApplicationClient.Cart.PlatformAddress: Hashable {
    public static func == (lhs: FDKClient.PlatformClient.ApplicationClient.Cart.PlatformAddress, rhs: FDKClient.PlatformClient.ApplicationClient.Cart.PlatformAddress) -> Bool {
        lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
       hasher.combine(id)
    }
}


extension String: Identifiable {
    public var id: String { return self }
}

extension FDKClient.ApplicationClient.Configuration.AppStaff: Hashable {
   public static func == (lhs: FDKClient.ApplicationClient.Configuration.AppStaff, rhs: FDKClient.ApplicationClient.Configuration.AppStaff) -> Bool {
       lhs.id == rhs.id
   }
   
   public func hash(into hasher: inout Hasher) {
      hasher.combine(id)
   }
}

extension FDKClient.ApplicationClient.Catalog.ProductSizePriceResponseV3: Hashable {
    public static func == (lhs: FDKClient.ApplicationClient.Catalog.ProductSizePriceResponseV3, rhs: FDKClient.ApplicationClient.Catalog.ProductSizePriceResponseV3) -> Bool {
        lhs.store?.uid == rhs.store?.uid
    }
    public func hash(into hasher: inout Hasher) {
      hasher.combine(store?.uid)
    }
}

extension FDKClient.ApplicationClient.Catalog.ProductSize: Hashable {
    public static func == (lhs: FDKClient.ApplicationClient.Catalog.ProductSize, rhs: FDKClient.ApplicationClient.Catalog.ProductSize) -> Bool {
        lhs.value == rhs.value
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(value)
    }
}


//MARK:PlatformClient.Order.OrderDetailsResponse + Equatable
extension FDKClient.PlatformClient.Order.OrderDetailsResponse: Hashable {
    public static func == (lhs: FDKClient.PlatformClient.Order.OrderDetailsResponse, rhs: FDKClient.PlatformClient.Order.OrderDetailsResponse) -> Bool {
        lhs.order?.fyndOrderId == rhs.order?.fyndOrderId
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(order?.fyndOrderId)
    }
}

extension FDKClient.PlatformClient.Order.PlatformShipment: Equatable {
   public static func == (lhs: FDKClient.PlatformClient.Order.PlatformShipment, rhs: FDKClient.PlatformClient.Order.PlatformShipment) -> Bool {
       lhs.shipmentId == rhs.shipmentId
   }
}

extension FDKClient.PlatformClient.Order.PlatformShipment: Hashable {
   public func hash(into hasher: inout Hasher) {
       hasher.combine(shipmentId)
   }
}


extension FDKClient.PlatformClient.Order.Reason: Equatable {
   public static func == (lhs: FDKClient.PlatformClient.Order.Reason, rhs: FDKClient.PlatformClient.Order.Reason) -> Bool {
       lhs.id == rhs.id
   }
}

extension FDKClient.PlatformClient.Order.Reason: Hashable {
   public func hash(into hasher: inout Hasher) {
       hasher.combine(id)
   }
}



// MARK: Extra
extension FDKClient.PlatformClient.Order.RefundModeInfo: Equatable {
    public static func == (lhs: FDKClient.PlatformClient.Order.RefundModeInfo, rhs: FDKClient.PlatformClient.Order.RefundModeInfo) -> Bool {
        lhs.slug == rhs.slug
    }
}

extension FDKClient.PlatformClient.Order.RefundModeInfo: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(slug)
    }
}

extension FDKClient.PlatformClient.Order.RefundOption: Equatable {
    public static func == (lhs: FDKClient.PlatformClient.Order.RefundOption, rhs: FDKClient.PlatformClient.Order.RefundOption) -> Bool {
        lhs.slug == rhs.slug
    }
}

extension FDKClient.PlatformClient.Order.RefundOption: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(slug)
    }
}

extension FDKClient.PlatformClient.Configuration.Application: Hashable {
    public static func == (lhs: FDKClient.PlatformClient.Configuration.Application, rhs: FDKClient.PlatformClient.Configuration.Application) -> Bool {
        lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(companyId)
        hasher.combine(id)
    }
}

